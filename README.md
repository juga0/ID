# Internal Deployment #

This is the code repository for the "Internal Deployment" project.   
Scripts and programs for automated build and packaging.

WIKI: https://dev.pep.foundation/CID/Internal%20Deployment

JIRA: https://pep.foundation/jira/projects/INDE/

for the time being.

Instructions:
-------------

- Build a portable RPM for Centos/RHEL 7/8

```bash
cd centos7/docker-build
make
```

RPM will will be put in `centos7/docker-build/rpm-builder/out/`

Install normally, using system package manager; eg. `yum install <package>.rpm`


- Package will install 1st-party libraries and `pEp.jar` to `/usr/local/lib`.
- 3rd-party library dependencies will be installed in `/usr/local/lib/pEp`
- `systemdb` will be in `/usr/local/share/pEp`.
- Headers will be located in `/usr/local/include/pEp`.
