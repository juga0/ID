#!/bin/bash

# Fully automated build script for the whole pEp software stack
#
# Last verfied to work: 26.8.2020 / MacOS 10.14 / Heck
# Last verfied to work: 26.8.2020 / Debian 10   / Heck
#
# Debian
# ======
# Required:
# * Debian 10 minimal install
# * To prepare your system:
# * run https://pep-security.lu/gitlab/cid/Internal-Deployment/-/blob/master/install-sys-deps-debian.sh to install all debian packages needed.
#
# MacOS
# ======
# Required:
# * MacOS 10.14 (newer versions might work)
# * System dependencies install instructions: https://dev.pep.foundation/Common%20Adapter%20Documentation/Adapter_Build_Instructions#macos
#
# TODO:
# If #BUILDDIR/example is already a git/mercurial repo
#   * If local copy is behind of changes
#      * Make clean fresh checkout
#      * make build, make install
#
#   otherwise just make install (always)
#
# * Dependency management (if sequoia is updated engine needs to be rebuilt, if engine has changes all adapters need to be rebuilt etc..)
# * Error handling
# * Port this script to:
#     * RHEL 6,7,8
#     * Fedora
# * Parallel build option

#set -x

function usage {
  echo -e '
   Fully automated build script for the whole pEp software stack

   usage: '$0' build-target build-root install-prefix

   This script will download and compile the latest version of the <build-target> from the repository,
   build it in <build-root> and install the resulting files in <install-prefix>.
   <build-root> and <install-prefix> will be created if necessary.

   Build Targets:
    * base ( sequoia, yml2, libetpan, asn1c, pEpEngine, libpEpAdapter)
    * sequoia
    * yml2
    * libetpan
    * asn1c
    * pEpEngine
    * libpEpAdapter
    * pEpPythonAdapter
    * pEpJNIAdapter

   Example:
   "'$0' base build dest"
   will download and compile all base sources in a dir structure under "build/" and
   install the resulting files in a dir structure under "dest/"

   THIS SCRIPT WILL ONLY WORK IN:
      - Debian 10
      - MacOS 10.14 (newer versions untested)
   '
}

# Helper function (realpath not existing on macos)
function realpath {
    python3 -c 'import sys, os; print(os.path.realpath(sys.argv[1]))' $1
}


# Check opts
if (( $# != 3 )); then
    usage
    exit
fi

# Global defs / get options
BUILD_TARGET=$(echo $1 | tr '[:upper:]' '[:lower:]')
BUILDROOT=$(realpath $2)
INSTPREFIX=$(realpath $3)


### Sequoia
function build_sequoia {
    echo -e "Building Sequoia... (go grab a coffee, or two... or three)"
    mkdir -p $BUILDROOT/sequoia
    cd $BUILDROOT/sequoia
    git clone https://gitlab.com/sequoia-pgp/sequoia.git .
    git checkout pep-engine
    make build-release PYTHON=disable
    make install PYTHON=disable PREFIX=$INSTPREFIX
    echo -e "Building Sequoia Finished\n"
}

### YML2
function build_yml2 {
    echo -e "Building YML2..."
    mkdir -p $BUILDROOT/yml2
    cd $BUILDROOT/yml2
    hg clone https://pep.foundation/dev/repos/yml2 .
    make
    echo -e "Building YML2 Finished\n"
}

### libetpan
function build_libetpan {
    echo -e "Building libetpan-fdik..."
    mkdir -p $BUILDROOT/libetpan
    cd $BUILDROOT/libetpan
    git clone https://github.com/fdik/libetpan .
    mkdir build
    ./autogen.sh --prefix=$INSTPREFIX
    make install
    echo -e "Building libetpan-fdik Finished\n"
}

### ASN1c
function build_asn1c {
    echo -e "Building ASN1c..."
    mkdir -p $BUILDROOT/asn1c
    cd $BUILDROOT/asn1c
    git clone git://github.com/vlm/asn1c.git .
    git checkout tags/v0.9.28 -b pep-engine
    autoreconf -iv
    ./configure --prefix=$INSTPREFIX
    make install
    echo -e "Building ASN1c Finished\n"
}

### pEpEngine
function build_pepengine {
    echo -e "Building pEpEngine..."
    mkdir -p $BUILDROOT/pEpEngine
    cd $BUILDROOT/pEpEngine
    hg clone https://pep.foundation/dev/repos/pEpEngine/ .
    echo '
PREFIX='$INSTPREFIX'
PER_MACHINE_DIRECTORY=$(PREFIX)/share/pEp

YML2_PATH='$BUILDROOT'/yml2

ETPAN_LIB=-L$(PREFIX)/lib
ETPAN_INC=-I$(PREFIX)/include

ASN1C=$(PREFIX)/bin/asn1c
ASN1C_INC=-I$(PREFIX)/share/asn1c

OPENPGP=SEQUOIA
SEQUOIA_LIB=-L$(PREFIX)/lib
SEQUOIA_INC=-I$(PREFIX)/include

export PKG_CONFIG_PATH=$(PREFIX)/share/pkgconfig/
    ' > local.conf
    make
    make install
    make db dbinstall
    echo -e "Building pEpEngine Finished\n"
}

### Test
function test_engine {
    echo -e "Checking existence of pEpEngine..."
    ls -l $INSTPREFIX/lib/libpEpEngine.so
    echo -e "Testing pEpEngine Finished\n"
}


## Adapters

### libpEpAdapter
function build_libpepadapter {
    echo -e "Building libpEpAdapter..."
    mkdir -p $BUILDROOT/libpEpAdapter
    cd $BUILDROOT/libpEpAdapter
    hg clone https://pep.foundation/dev/repos/libpEpAdapter/ .
    echo '
      ENGINE_LIB_PATH='$INSTPREFIX'/lib
      ENGINE_INC_PATH='$INSTPREFIX'/include
    ' > local.conf
    make install PREFIX=$INSTPREFIX
    echo -e "Building libpEpAdapter Finished\n"
}

### pEpPythonAdapter
function build_peppythonadapter {
    echo -e "Building pEpPythonAdapter..."
    mkdir -p $BUILDROOT/pEpPythonAdapter
    cd $BUILDROOT/pEpPythonAdapter
    hg clone https://pep.foundation/dev/repos/pEpPythonAdapter/ .
    python3 setup.py build_ext --prefix=$INSTPREFIX
    echo -e "Building pEpPythonAdapter Finished\n"
}

### Test
function test_peppythonadapter {
    echo -e "Testing pEpPythonAdapter..."
    export LD_LIBRARY_PATH=$INSTPREFIX/lib
    export PYTHONPATH=$BUILDROOT/pEpPythonAdapter/build/lib.linux-x86_64-3.7/
    python3 -c 'import pEp;'
    echo -e "Testing pEpPythonAdapter Finished\n"
}

### pEpJNIAdapter
function build_pepjniadapter {
    echo -e "Building pEpJNIAdapter..."
    mkdir -p $BUILDROOT/pEpJNIAdapter
    cd $BUILDROOT/pEpJNIAdapter
    hg clone https://pep.foundation/dev/repos/pEpJNIAdapter/ .
    echo '
        PREFIX='$INSTPREFIX'
        YML2_PATH='$BUILDROOT'/yml2
        ENGINE_LIB_PATH=$(PREFIX)/lib
        ENGINE_INC_PATH=$(PREFIX)/include
        AD_LIB_PATH=$(PREFIX)/lib
        AD_INC_PATH=$(PREFIX)/include
    ' > local.conf
    make
    echo -e "Building pEpJNIAdapter Finished\n"
}

### Test
function test_pepjniadapter {
    echo -e "Testing pEpJNIAdapter..."
    cd $BUILDROOT/pEpJNIAdapter/test/java/foundation/pEp/jniadapter/test/basic
    export LD_LIBRARY_PATH=$INSTPREFIX/lib
    make
    echo -e "Testing pEpJNIAdapter Finished\n"
}


function main {
      # Create basic dirs
      mkdir -p $BUILDROOT
      mkdir -p $INSTPREFIX

      # Setup Env
      echo -e "Setting up env..."

      # PATH
      export PATH=$PATH:$BUILDROOT/yml2
      echo $PATH

      # JAVA_HOME (only for the pEpJNIAdapter)
      if [ $(uname) == "Linux" ]; then {
        export JAVA_HOME=$(dirname $(dirname $(readlink -f /usr/bin/javac)));
      } fi
      if [ $(uname) == "Darwin" ]; then {
        # Shitty "2nd-hand-fruit OS" makes it difficult to determine JAVA_HOME
        export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_231.jdk/Contents/Home;
      } fi
      echo JAVA_HOME:$JAVA_HOME

      echo -e "Setting up env Finished!\n"

      # Choose Build Target
      case "$BUILD_TARGET" in
        'base')
          build_sequoia
          build_yml2
          build_libetpan
          build_asn1c
          build_pepengine
          build_libpepadapter
          ;;
        'sequoia')
          build_sequoia
          ;;
        'yml2')
          build_yml2
          ;;
        'libetpan')
          build_libetpan
          ;;
        'asn1c')
          build_asn1c
          ;;
        'pepengine')
          build_pepengine
          ;;
        'libpepadapter')
          build_libpepadapter
          ;;
        'peppythonadapter')
          echo 'make sure you already built pEp-libs-base'
          build_peppythonadapter
          test_peppythonadapter
          ;;
        'pepjniadapter')
          echo 'make sure you already built pEp-libs-base'
          build_pepjniadapter
          test_pepjniadapter
          ;;
        *)
          usage
          ;;
      esac
}

main