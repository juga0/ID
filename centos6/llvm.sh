#!/bin/bash
set -exuo pipefail

cd ${BUILDROOT}
for COMP in cfe:tools/clang libcxx:projects/libcxx libcxxabi:projects/libcxxabi lld:tools/lld ; do  # also add to -DLLVM_ENABLE_PROJECTS= below
    mkdir -p ${BUILDROOT}/llvm/llvm-${llvm_ver}.src/${COMP#*:}
    tar -C ${BUILDROOT}/llvm/llvm-${llvm_ver}.src/${COMP#*:} -xf ${BUILDROOT}/${COMP%:*}-${llvm_ver}.src.tar.xz --strip-components=1
    rm ${BUILDROOT}/${COMP%:*}-${llvm_ver}.src.tar.xz
done
