#!/bin/bash
set -exuo pipefail

export LC_ALL=en_US.UTF-8 && \

# ln -sf ${INSTPREFIX}/pep/include/pEp/sync_api.h ${INSTPREFIX}/pep/include/pEp/sync.h
# sed -i 's/sync.h/Sync.h/' org_pEp_jniadapter_AbstractEngine.cc  || true
# sed -i 's/sync.h/Sync.h/' basic_api.cc  || true

# JAVA_HOME (only for the pEpJNIAdapter)
if [ $(uname) == "Linux" ]; then {
  export JAVA_HOME=$(dirname $(dirname $(readlink -f /usr/bin/javac)));
} fi
if [ $(uname) == "Darwin" ]; then {
  export JAVA_HOME=$(dirname $(dirname $(readlink /usr/bin/javac)));
} fi
echo $JAVA_HOME

cat >local.conf <<__LOCAL__
PREFIX    = ${INSTPREFIX}
#JAVA_HOME=${JAVA_HOME} 
JAVA_HOME = /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.242.b08-0.el7_7.x86_64
YML2_PATH=${BUILDROOT}/yml2
YML2_PROC=${BUILDROOT}/yml2/yml2proc --encoding=utf8
#YML2_OPTS=--encoding=utf8
ENGINE_INC= -I${INSTPREFIX}/include
ENGINE_LIB= -L${INSTPREFIX}/lib
AD_INC    = -I${INSTPREFIX}/include
AD_LIB    = -L${INSTPREFIX}/lib

CC        = clang
CXX       = clang++ -stdlib=libc++ -std=c++11
CXXFLAGS +=  -I${INSTPREFIX}/llvm/include/c++/v1 -I${BUILDROOT}/pEpEngine/src -stdlib=libc++ \
    -isystem ${INSTPREFIX}/include -isystem ${INSTPREFIX}/include
# CPPFLAGS  = -isystem ${INSTPREFIX}/pep/include # -isystem /usr/include
# -isystem ${INSTPREFIX}/pep/include/pEp 
CFLAGS   +=  -isystem ${INSTPREFIX}/include -isystem ${INSTPREFIX}/asn1c/share/asn1c

# -isystem ${INSTPREFIX}/libpepa/include -isystem ${INSTPREFIX}/pep/include
LDFLAGS  += \
    -L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/lib -Wl,-rpath,${INSTPREFIX}/lib -L${INSTPREFIX}/lib -Wl,-rpath,${INSTPREFIX}/lib \
    -UDISABLE_SYNC -fuse-ld=lld

# LDFLAGS   = -L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/pep/lib -Wl,-rpath,${INSTPREFIX}/pep/lib -fuse-ld=lld
# -nostartfiles
__LOCAL__
