FROM pep-centos7-engine:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_MACHINE_DIR /usr/local/share/pEp


### libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ $BUILDROOT/libpEpAdapter && \
    cd $BUILDROOT/libpEpAdapter && \
    echo "ENGINE_LIB_PATH=${INSTPREFIX}/lib" >> local.conf && \
    echo "ENGINE_INC_PATH=${INSTPREFIX}/include" >> local.conf && \
    source scl_source enable devtoolset-8 && \
    export LC_ALL=en_US.UTF-8 && \
   # export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
  make && \
  make install PREFIX=$INSTPREFIX


## JNI Adapter
USER root
RUN yum -y install time java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
    yum clean all
USER pep-builder
COPY JNI.sh /usr/bin/JNI.sh
RUN hg clone https://pep.foundation/dev/repos/pEpJNIAdapter ${BUILDROOT}/pEpJNIAdapter && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    hg up -Cr sync && \
    hg purge -X local.conf --all --config extensions.purge= && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    export LC_ALL=en_US.UTF-8 && \
    export pepjni_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    #source scl_source enable devtoolset-8 && \
    #export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    #export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    #export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:/opt/rh/python27${BUILDROOT}/usr/lib64 && \
    #export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/sequoia/usr/share/pkgconfig:/opt/rh/python27${BUILDROOT}/usr/lib64/pkgconfig && \
    cd ${BUILDROOT}/pEpJNIAdapter/src && \
    mkdir -p ${INSTPREFIX}/home/java && \ 
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${INSTPREFIX}/libetpan/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
    bash JNI.sh && \
    # make clean && \
    # For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here, && \
    #  instead add them to local.conf ( yes, home-cooked build-system :-/ ) && \
    make WARN= DEBUG= && \
    #cd ${BUILDROOT}/pEpJNIAdapter/test && \
    #make && \
    # make WARN= DEBUG= install && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    install -m 644 -t ${INSTPREFIX}/lib src/libpEpJNI.a && \
    install -m 755 -t ${INSTPREFIX}/lib src/libpEpJNI.so && \
    install -m 644 -t ${INSTPREFIX}/lib src/pEp.jar && \
    echo "${pepjni_ver}">${INSTPREFIX}/pEp_JNI.ver


### Test JNI (Currently fails, hence commented out...)
    #echo -e "Testing pEpJNIAdapter..." && \
    #cd $BUILDROOT/pEpJNIAdapter/test/Basic && \
    ##export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${INSTPREFIX}/libetpan/lib:${INSTPREFIX}/nettle/lib64:/usr/lib64 && \
    #make && \
    #echo -e "Testing pEpJNIAdapter Finished\n"
