#!/bin/bash
set -exuo pipefail

export capnp_ver=0.7.0

cd ${BUILDROOT}/capnproto-c++-${capnp_ver}

patch -p1 <<EOF
diff --git a/src/kj/table.c++ b/src/kj/table.c++
index 5f363e0..772adac 100644
--- a/src/kj/table.c++
+++ b/src/kj/table.c++
@@ -22,6 +22,7 @@
 #include "table.h"
 #include "debug.h"
 #include <stdlib.h>
+#include <malloc.h>

 namespace kj {
 namespace _ {
@@ -307,7 +308,7 @@ void BTreeImpl::growTree(uint minCapacity) {
 #else
   // Let's use the C11 standard.
   NodeUnion* newTree = reinterpret_cast<NodeUnion*>(
-      aligned_alloc(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
+      memalign(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
   KJ_ASSERT(newTree != nullptr, "memory allocation failed", newCapacity);
 #endif
EOF
