#!/bin/bash
set -exuo pipefail

# ===========================
# Distro
# ===========================

echo 7 >"${BUILDROOT}/D_REVISION"

D_REV=$(cat ${BUILDROOT}/D_REVISION)
D=""

D=${BUILDROOT}/out
# trap '[ -z "$D" ] || rm -rf "$D"' EXIT
#D=$(mktemp -d)

mkdir -p ${BUILDROOT}/out
rm -rf ${BUILDROOT}/out/*
# pep  asn1c  capnp  cmake  curl  gmp  llvm  nettle  ninja  sequoia
# bin  include  lib  lib64  libexec  share
mkdir -p "$D"/{bin,lib/pEp,share/pEp,include/pEp}

# Engine and below, and libpEpAdapter
cp -a ${INSTPREFIX}/lib/libpEpEngine.so "$D"/lib
cp -ar ${INSTPREFIX}/libetpan/lib/libetpan.so* "$D"/lib/pEp
cp -ar ${INSTPREFIX}/sqlite3/lib/libsqlite3.so* "$D"/lib/pEp

cp -arv ${INSTPREFIX}/include/pEp/. "$D"/include/pEp

cp -arv /opt/pEp/share/system.db "$D"/share

# SQlite CLI
cp -a ${INSTPREFIX}/sqlite3/bin/sqlite3 "$D"/bin

# Sequoia cmdline (optional above)
if [ -f ${INSTPREFIX}/bin/sq ] ; then
  cp -a ${INSTPREFIX}/lib/libsequoia_*.so* "$D"/lib/pEp
  cp -a ${INSTPREFIX}/bin/sq "$D"/bin
  cp -a ${INSTPREFIX}/bin/sqv "$D"/bin
else
  cp -a ${INSTPREFIX}/sequoia/lib/libsequoia_openpgp_ffi.so* "$D"/lib/pEp
fi

cp -a ${INSTPREFIX}/nettle/lib64/libnettle.so* "$D"/lib/pEp
cp -a ${INSTPREFIX}/nettle/lib64/libhogweed.so* "$D"/lib/pEp
cp -a ${INSTPREFIX}/gmp/lib/libgmp.so* "$D"/lib/pEp
cp -af ${INSTPREFIX}/llvm/lib/libc++.so* "$D"/lib/pEp
cp -af ${INSTPREFIX}/llvm/lib/libc++abi.so* "$D"/lib/pEp

# pEpJNI
cp -a ${INSTPREFIX}/lib/libpEpJNI.a "$D"/lib
cp -a ${INSTPREFIX}/lib/libpEpJNI.so "$D"/lib
cp -a ${INSTPREFIX}/lib/pEp.jar "$D"/lib

#: > "$D"/share/pEp/VERSIONS.txt
#for VER in ${INSTPREFIX}/*.ver ; do 
#    _var="$(basename "$VER" .ver)"
#    echo "$_var: $(cat "$VER")" >>"$D"/share/pEp/VERSIONS.txt
#    eval "VER_${_var}=$(cat "$VER")"
#done

find "$D"/lib -maxdepth 1 -type f -print -exec patchelf --set-rpath '$ORIGIN/pEp:$ORIGIN' {} \;
find "$D"/lib/pEp         -type f -print -exec patchelf --set-rpath '$ORIGIN' {} \;
find "$D"/bin -type f -print -exec patchelf --set-rpath '$ORIGIN/../lib/pEp:$ORIGIN/../lib' {} \;

ls -lh "$D"/*
du -sch "$D"

eval VER_pEp_JNI=$(cat ${INSTPREFIX}/pEp_JNI.ver)
#VER_sq=$(cat ${INSTPREFIX}/pEp_JNI.ver)
eval VER_pEp_engine=$(cat ${INSTPREFIX}/pEp_engine.ver)

#TARNAME="JNI-Adapter_r${D_REV}-${VER_pEp_JNI}+Sequoia-${VER_sq}+pEpEngine-${VER_pEp_engine}.tar.bz2"
TARNAME="JNI-Adapter_r${D_REV}-${VER_pEp_JNI}+Sequoia+pEpEngine-${VER_pEp_engine}.tar.bz2"
tar -C "$D" -cJvf "${OUTDIR}/${TARNAME}" .
