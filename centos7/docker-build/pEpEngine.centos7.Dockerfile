FROM pep-centos7-sequoia:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_MACHINE_DIR /usr/local/share/pEp


### YML2
RUN cd $BUILDROOT && \
    wget https://fdik.org/yml2.tar.bz2 && \
    tar -xf yml2.tar.bz2 && \
    rm yml2.tar.bz2


### libetpan
RUN git clone https://github.com/fdik/libetpan $BUILDROOT/libetpan && \
    cd $BUILDROOT/libetpan && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    test -f configure || NOCONFIGURE=absolutely ./autogen.sh && \
    ./configure --prefix=${INSTPREFIX}/libetpan \
        --without-openssl --without-gnutls --without-sasl \
        --without-curl --without-expat --without-zlib \
        --disable-dependency-tracking \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -I${INSTPREFIX}/libiconv/include -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld" && \ 
    make -j$(nproc) && \
    make install && \
    echo "${libetpan_ver}">${INSTPREFIX}/libetpan.ver


### ASN1c Requirements
RUN export m4_ver=1.4.18 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/m4/m4-${m4_ver}.tar.gz \
        -O ${BUILDROOT}/m4-${m4_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf m4-${m4_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/m4-${m4_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${m4_ver}">${INSTPREFIX}/m4.ver


RUN export libtool_ver=2.4.6 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/libtool/libtool-${libtool_ver}.tar.gz \
        -O ${BUILDROOT}/libtool-${libtool_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf libtool-${libtool_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/libtool-${libtool_ver} && \
    ./configure --with-sysroot=${INSTPREFIX}/sysroot --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${libtool_ver}">${INSTPREFIX}/libtool.ver
    
    
RUN export autoconf_ver=2.69 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/autoconf/autoconf-${autoconf_ver}.tar.gz \ 
        -O ${BUILDROOT}/autoconf-${autoconf_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf autoconf-${autoconf_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/autoconf-${autoconf_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${autoconf_ver}">${INSTPREFIX}/autoconf.ver
    
    
RUN export automake_ver=1.16.1 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/automake/automake-${automake_ver}.tar.gz \
        -O ${BUILDROOT}/automake-${automake_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf automake-${automake_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/automake-${automake_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${automake_ver}">${INSTPREFIX}/automake.ver
    
    
RUN export pkg_config_ver=0.29.2 && \
    cd ${BUILDROOT} && \
    wget https://pkg-config.freedesktop.org/releases/pkg-config-${pkg_config_ver}.tar.gz \
        -O ${BUILDROOT}/pkg-config-${pkg_config_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf pkg-config-${pkg_config_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/pkg-config-${pkg_config_ver} && \
    ./configure  --with-internal-glib --with-sysroot=${INSTPREFIX}/sysroot --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${pkg_config_ver}">${INSTPREFIX}/pkg_config.ver


### ASN1c
RUN git clone https://github.com/vlm/asn1c.git $BUILDROOT/asn1c && \
    cd $BUILDROOT/asn1c && \
    git checkout tags/v0.9.28 -b pep-engine && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/autotools/lib && \
    test -f configure || autoreconf -iv && \
    ./configure --prefix=${INSTPREFIX}/asn1c \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld" && \ 
    make -j$(nproc) && make install && \
    echo "${asn1c_ver}">${INSTPREFIX}/asn1c.ver


RUN export sqlite3_ver=3280000 && \
    export sqlite3_ver=3290000 && \
    cd ${BUILDROOT} && \
    wget https://sqlite.org/2019/sqlite-src-${sqlite3_ver}.zip \
        -O ${BUILDROOT}/sqlite-src-${sqlite3_ver}.zip && \
    rm -rf ${BUILDROOT}/sqlite-src-${sqlite3_ver} && \
    ( cd ${BUILDROOT} ; unzip -x ${BUILDROOT}/sqlite-src-${sqlite3_ver}.zip ) && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/sqlite-src-${sqlite3_ver} && \
    ./configure --prefix=${INSTPREFIX}/sqlite3 \
        --disable-tcl --disable-amalgamation --disable-readline --enable-debug \
        --enable-session \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld"  && \
    make -j$(nproc) && \
    make install && \
    echo "${sqlite3_ver}">${INSTPREFIX}/sqlite3.ver


### pEpEngine
#COPY pEpEngine.local.conf ${BUILDROOT}
RUN cd ${BUILDROOT} && \
    echo "PREFIX=$INSTPREFIX" >> local.conf && \
    echo 'SQLITE3_FROM_OS=""' >> local.conf && \
    echo "PER_MACHINE_DIRECTORY=$PEP_MACHINE_DIR" >> local.conf && \
    echo "YML2_PATH=${BUILDROOT}/yml2" >> local.conf && \
    echo "YML2_PROC=${BUILDROOT}/yml2/yml2proc" >> local.conf && \
    echo "ETPAN_LIB=-L${INSTPREFIX}/libetpan/lib" >> local.conf && \
    echo "ETPAN_INC=-I${INSTPREFIX}/libetpan/include" >> local.conf && \
    echo "#ASN1C=${INSTPREFIX}/asn1c/bin/asn1c" >> local.conf && \
    echo "ASN1C_INC=-I${INSTPREFIX}/asn1c/share/asn1c" >> local.conf && \
    echo "OPENPGP=SEQUOIA" >> local.conf && \
    echo "SEQUOIA_LIB=-L${INSTPREFIX}/lib" >> local.conf && \
    echo "SEQUOIA_INC=-I${INSTPREFIX}/include" >> local.conf && \
    echo "CC        = clang" >> local.conf && \
    echo "CXX       = clang++ -stdlib=libc++ -std=c++11" >> local.conf && \
    echo "CXXFLAGS  = -I${INSTPREFIX}/llvm/include/c++/v1 -I/opt/rh/devtoolset-8/root/usr/include/c++/8  -I/opt/rh/devtoolset-8/root/usr/include/c++/8/x86_64-redhat-linux/  -I${BUILDROOT}/pEpEngine/src -stdlib=libc++" >> local.conf && \
    echo "CPPFLAGS += -USYSTEM_DB -isystem ${INSTPREFIX}/asn1c/share/asn1c -I${INSTPREFIX}/sqlite3/include -I${INSTPREFIX}/libetpan/include -isystem ${INSTPREFIX}/pep/include -isystem /usr/include" >> local.conf && \
    echo "LDFLAGS  += -LINSTPREFIX}/llvm/lib -L${INSTPREFIX}/lib -L${INSTPREFIX}/libetpan/lib -L${INSTPREFIX}/sqlite3/lib -L${INSTPREFIX}/pep/lib -fuse-ld=lld -nostartfiles" >> local.conf


RUN hg clone https://pep.foundation/dev/repos/pEpEngine $BUILDROOT/pEpEngine && \
    cd $BUILDROOT/pEpEngine && \
    hg up sync && \
    export engine_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    mv ${BUILDROOT}/local.conf ${BUILDROOT}/pEpEngine/local.conf && \
    cat local.conf && \
    source scl_source enable devtoolset-8 && \
    export LC_ALL=en_US.UTF-8 && \
   # export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:/opt/rh/python27${BUILDROOT}/usr/lib64:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
    make && make install && \
    echo "Setup DB" && \
    make -C db install && \
    echo "${engine_ver}">${INSTPREFIX}/pEp_engine.ver
