#!/bin/bash
set -exuo pipefail

# ===========================
# Distro
# ===========================

echo 7 >"${INSTPREFIX}/D_REVISION"

D_REV=$(cat ${INSTPREFIX}/D_REVISION)
D=""

D=${INSTPREFIX}/out
# trap '[ -z "$D" ] || rm -rf "$D"' EXIT
#D=$(mktemp -d)

mkdir -p ${INSTPREFIX}/out
rm -rf ${INSTPREFIX}/out/*
# pep  asn1c  capnp  cmake  curl  gmp  llvm  nettle  ninja  sequoia
# bin  include  lib  lib64  libexec  share
mkdir -p "$D"/{bin,ld,lib/pEp,share/pEp,include/pEp}

# Engine and below, and libpEpAdapter
cp -a ${INSTPREFIX}/lib/libpEpEngine.so "$D"/lib
cp -ar ${INSTPREFIX}/libetpan/lib/libetpan.so* "$D"/lib/pEp
cp -ar ${INSTPREFIX}/sqlite3/lib/libsqlite3.so* "$D"/lib/pEp

cp -arv ${INSTPREFIX}/include/pEp/. "$D"/include/pEp

cp -arv /usr/local/share/pEp/system.db "$D"/share/pEp

# SQlite CLI
cp -a ${INSTPREFIX}/sqlite3/bin/sqlite3 "$D"/bin

# Sequoia cmdline (optional above)
if [ -f ${INSTPREFIX}/bin/sq ] ; then
  cp -a ${INSTPREFIX}/lib/libsequoia_*.so* "$D"/lib/pEp
  cp -a ${INSTPREFIX}/bin/sq "$D"/bin
  cp -a ${INSTPREFIX}/bin/sqv "$D"/bin
else
  cp -a ${INSTPREFIX}/sequoia/lib/libsequoia_openpgp_ffi.so* "$D"/lib/pEp
fi

cp -a ${INSTPREFIX}/nettle/lib64/libnettle.so* "$D"/lib/pEp
cp -a ${INSTPREFIX}/nettle/lib64/libhogweed.so* "$D"/lib/pEp
cp -a ${INSTPREFIX}/gmp/lib/libgmp.so* "$D"/lib/pEp
cp -af ${INSTPREFIX}/llvm/lib/libc++.so* "$D"/lib/pEp
cp -af ${INSTPREFIX}/llvm/lib/libc++abi.so* "$D"/lib/pEp

# pEpJNI
cp -a ${INSTPREFIX}/lib/libpEpJNI.a "$D"/lib
cp -a ${INSTPREFIX}/lib/libpEpJNI.so "$D"/lib
cp -a ${INSTPREFIX}/lib/pEp.jar "$D"/lib

# ld config

cp -ar ${INSTPREFIX}/ld/usr-local.conf "$D"/ld

# versions
cp -a ${INSTPREFIX}/*.ver "$D"

#: > "$D"/share/pEp/VERSIONS.txt
#for VER in ${INSTPREFIX}/*.ver ; do 
#    _var="$(basename "$VER" .ver)"
#    echo "$_var: $(cat "$VER")" >>"$D"/share/pEp/VERSIONS.txt
#    eval "VER_${_var}=$(cat "$VER")"
#done

find "$D"/lib -maxdepth 1 -type f -print -exec patchelf --set-rpath '$ORIGIN/pEp:$ORIGIN' {} \;
find "$D"/lib/pEp         -type f -print -exec patchelf --set-rpath '$ORIGIN' {} \;
find "$D"/bin -type f -print -exec patchelf --set-rpath '$ORIGIN/../lib/pEp:$ORIGIN/../lib' {} \;

ls -lh "$D"/*
du -sch "$D"
