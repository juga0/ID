#!/bin/bash
set -exuo pipefail

#source scl_source enable devtoolset-8
#export PATH=${INSTPREFIX}/curl/bin:${INSTPREFIX}/llvm/bin:$PATH
### export RUSTUP_UPDATE_ROOT=file://${BUILDROOT}/rustup.dl
## mkdir -p ${BUILDROOT}/rustup.dl/dist/x86_64-unknown-linux-gnu/rustup-init"
#export CC=$(which gcc)
#export CXX=$(which g++)
export _arch=x86_64-unknown-linux-gnu
export _rustup_init=rustup.dl/dist/${_arch}/rustup-init

if [ -x "$_rustup_init" ] ; then \
  "$_rustup_init" -y -v --no-modify-path --default-host x86_64-unknown-linux-gnu 
else 
  if [ ! -x ${BUILDROOT}/rustup ] ; then 
    wget https://sh.rustup.rs -O ${BUILDROOT}/rustup 
  fi 
  bash -vx ${BUILDROOT}/rustup -y 
fi
