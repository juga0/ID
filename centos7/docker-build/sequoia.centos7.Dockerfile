FROM pep-centos7-toolchain:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_MACHINE_DIR /usr/local/share/pEp

# ============================================
# Things to compile with clang
# ============================================

# Install Sequoia dependency "capnproto" from source
#  -- following to https://capnproto.org/install.html, slightly adapted
COPY capnp_patch.sh /usr/bin/capnp_patch.sh

RUN export capnp_ver=0.7.0 && \
    cd ${BUILDROOT} && \
    #rm -rf  ${INSTPREFIX}/capnp && \
    mkdir -p ${INSTPREFIX}/capnp && \
    wget https://capnproto.org/capnproto-c++-${capnp_ver}.tar.gz \
        -O ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    # rm -rf ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    rm ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    cd ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    bash capnp_patch.sh



# ATTENTION ATTENTION ATTENTION
# =============================
#
# The line src/kj/filesystem-disk-test.c++ (tests fail)
#
# But, also that we disable sequoia-store in sequoia.
#
RUN source scl_source enable devtoolset-8 && \
    export capnp_ver=0.7.0 && \
    export PATH=${INSTPREFIX}/curl/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    ./configure --prefix=${INSTPREFIX}/capnp \
        CC=clang \
        CXX=clang++ "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++ " \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/capnp/lib -fuse-ld=lld" && \
    make -j$(nproc) all && \
    : > src/kj/filesystem-disk-test.c++ && \
    make -j$(nproc) check && \
    make install && \
    echo "${capnp_ver}">${INSTPREFIX}/capnp.ver



##
## QA:
##  * Need LD_RUN_PATH and LD_LIBRARY_PATH to lib-path of LLVM (or configure steps will fail)
##  * Need CC=clang and CXX=clang++
##  * CXXFLAGS needs: -I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++
##  * Repeat -stdlib=libc++ in both CXXFLAGS and "CXX=clang++ -stdlib=libc++"
##  * Add to LDFLAGS: -L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib
##  * Add any other local lib to LDFLAGS: -L<lib-path> -Wl,-rpath,<lib-path>
##  * Append to LDFLAGS: -Wl,-rpath,<lib-prefix>
##  * Append to LDFLAGS: -fuse-ld=lld
##  * Add local lib's pkgconfig directories to PKG_CONFIG_PATH
##
## Some docu for C++: https://libcxx.llvm.org/docs/UsingLibcxx.html
## https://wiki.musl-libc.org/building-llvm.html
## 
#
#
#
RUN export gmp_ver=6.1.2 && \
    cd ${BUILDROOT} && \
    wget https://gmplib.org/download/gmp/gmp-${gmp_ver}.tar.bz2 \
        -O ${BUILDROOT}/gmp-${gmp_ver}.tar.bz2 && \
    # rm -rf ${BUILDROOT}/gmp-${gmp_ver}
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/gmp-${gmp_ver}.tar.bz2 && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/gmp-${gmp_ver} && \
    ./configure --prefix=${INSTPREFIX}/gmp \
        --enable-cxx \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib -fuse-ld=lld" && \
    make -j$(nproc) && \
    make install && \
    echo "${gmp_ver}">${INSTPREFIX}/gmp.ver



RUN export nettle_ver=3.4 && \
    export nettle_ver=3.5 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/nettle/nettle-${nettle_ver}.tar.gz \
        -O ${BUILDROOT}/nettle-${nettle_ver}.tar.gz && \
    # rm -rf ${BUILDROOT}/nettle-${nettle_ver}
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/nettle-${nettle_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/curl/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/nettle-${nettle_ver} && \
    ./configure --prefix=${INSTPREFIX}/nettle \
        --enable-x86-aesni \
        CC=clang CFLAGS="-I${INSTPREFIX}/gmp/include" \
        CXX=please-fail \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/nettle/lib64 -fuse-ld=lld"  && \
    make -j$(nproc) && \
    make install && \
    echo "${nettle_ver}">${INSTPREFIX}/nettle.ver


### Sequoia
RUN git clone https://gitlab.com/sequoia-pgp/sequoia.git -b pep-engine && \
    cd sequoia && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/cmake/bin:${INSTPREFIX}/curl/bin:${INSTPREFIX}/nettle/bin:${INSTPREFIX}/capnp/bin:${INSTPREFIX}/llvm/bin:$HOME/.cargo/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/nettle/lib64/pkgconfig && \
    pkg-config --libs-only-L nettle hogweed && \
   # export _Wl_rpath=$(pkg-config --libs-only-L nettle hogweed) && \
    #export _Wl_rpath=${_Wl_rpath//-L//-Wl,-rpath,/} && \
    export CC=clang && \
    export CFLAGS="-I${INSTPREFIX}/gmp/include $(pkg-config --cflags nettle hogweed) -I${INSTPREFIX}/pep/include" && \
    export CXX="clang++ -stdlib=libc++" && \
    export CXXFLAGS="-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++ -I${INSTPREFIX}/gmp/include $(pkg-config --cflags nettle hogweed) -I${INSTPREFIX}/pep/include" && \
    export LDFLAGS="-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib $(pkg-config --libs-only-L nettle hogweed) ${_Wl_rpath} -L${INSTPREFIX}/pep/lib -Wl,-rpath,${INSTPREFIX}/pep/lib -L"${sq_src}"/target/release -Wl,-rpath,${INSTPREFIX}/sequoia/lib -fuse-ld=lld" && \
    #export _rust_L="-Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib ${_Wl_rpath} -Wl,-rpath,${INSTPREFIX}/pep/lib -Wl,-rpath,${INSTPREFIX}/sequoia/lib" && \
    #export _rust_L=${_rust_L-Wl,-C link-arg=-Wl,} && \
    export RUSTFLAGS=" -L${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib $(pkg-config --libs-only-L nettle hogweed) -L${INSTPREFIX}/pep/lib" && \
    #source $HOME/.cargo/env && \
    make install PYTHON=disable PREFIX=$INSTPREFIX
