FROM centos:centos7 

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_MACHINE_DIR /usr/local/share/pEp

  # Create basic dirs
RUN mkdir -p ${BUILDROOT}
RUN mkdir -p ${INSTPREFIX}
RUN mkdir -p ${OUTDIR}
RUN mkdir -p ${PEP_MACHINE_DIR}

  # Setup Env
ENV PATH = ${PATH}:${DIR_YML2}

WORKDIR /build

RUN yum -y install patch git cmake yum-utils rpm-build rpmdevtools \
                   openssl-devel zlib-devel epel-release \
	           libxml2-devel gmp gmp-devel sqlite-devel

RUN yum -y install centos-release-scl

RUN yum -y install devtoolset-8 devtoolset-8-gcc devtoolset-8-gcc-c++ \
                   python36 python36-lxml mercurial libuuid \
		   libuuid-devel automake libtool tcl which patchelf \ 
		   rustc cargo make pkg-config nettle-devel \
		   capnproto wget && \
                   yum clean all


RUN adduser -U -r -s /bin/sh pep-builder && \
    mkdir -p /home/pep-builder/.cargo

RUN chown -R pep-builder:pep-builder ${BUILDROOT} && \
    chown -R pep-builder:pep-builder ${INSTPREFIX} && \
    chown -R pep-builder:pep-builder ${OUTDIR} && \
    chown -R pep-builder:pep-builder ${PEP_MACHINE_DIR} && \
    chown -R pep-builder:pep-builder /home/pep-builder

USER pep-builder

RUN export cmake_ver=3.14.3 && \
    export cmake_ver=3.14.5 && \
    cd ${BUILDROOT} && \
    wget https://github.com/Kitware/CMake/releases/download/v${cmake_ver}/cmake-${cmake_ver}.tar.gz \
        -O ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    rm ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    cd ${BUILDROOT}/cmake-${cmake_ver} && \
    source scl_source enable devtoolset-8 && \
    MAKEFLAGS=-j$(nproc) ./bootstrap --prefix=${INSTPREFIX}/cmake -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make -j$(nproc) && \
    make install && \
    echo \${cmake_ver}>${INSTPREFIX}/cmake.ver

RUN export ninja_ver=1.9.0 && \
    cd ${BUILDROOT} && \
    wget https://github.com/ninja-build/ninja/archive/v${ninja_ver}.tar.gz \
        -O ninja-${ninja_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf ninja-${ninja_ver}.tar.gz && \
    rm ninja-${ninja_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    cd ${BUILDROOT}/ninja-${ninja_ver} && \
    python configure.py --verbose --bootstrap && \
    mkdir -p ${INSTPREFIX}/ninja/bin && \
    install -m 755 ninja ${INSTPREFIX}/ninja/bin/ninja && \
    echo \${ninja_ver}>${INSTPREFIX}/ninja.ver



# Credits go to many, and in particular:
#   https://shaharmike.com/cpp/build-clang/

COPY llvm.sh /usr/bin/llvm.sh

RUN export llvm_ver=8.0.0 && \
    cd ${BUILDROOT} && \
    wget https://releases.llvm.org/${llvm_ver}/llvm-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/libcxx-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/libcxxabi-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/lld-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/lldb-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/cfe-${llvm_ver}.src.tar.xz && \
    mkdir -p ${BUILDROOT}/llvm && \
    tar -C ${BUILDROOT}/llvm -xf ${BUILDROOT}/llvm-${llvm_ver}.src.tar.xz && \
    rm ${BUILDROOT}/llvm-${llvm_ver}.src.tar.xz && \
    /bin/bash -c llvm.sh && \
    mkdir -p ${INSTPREFIX}/llvm && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/cmake/bin:${INSTPREFIX}/ninja/bin:$PATH && \
    export CC=$(which gcc) && \
    export CXX=$(which g++) && \
    export PYEXE=$(which python) && \
    mkdir -p ${BUILDROOT}/llvm/build/ && \
    cd ${BUILDROOT}/llvm/build/ && \
    cmake ../llvm-${llvm_ver}.src -GNinja \
      "-DCMAKE_CXX_COMPILER=$CXX" "-DCMAKE_C_COMPILER=$CC" "-DPYTHON_EXECUTABLE=$PYEXE" \
      -DLLVM_TARGETS_TO_BUILD=X86 \
      -DLLVM_INCLUDE_EXAMPLES=OFF -DLLVM_INCLUDE_DOCS=OFF \
      -DLIBCXX_ENABLE_SHARED=YES -DLIBCXX_ENABLE_STATIC=NO \
      -DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=NO \
      -DLIBCXX_CXX_ABI=libcxxabi \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=${INSTPREFIX}/llvm \
      -DLIBCXX_CXX_ABI=libcxxabi \
      -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${BUILDROOT}/llvm/build/include/c++/v1;${BUILDROOT}/llvm/llvm-${llvm_ver}.src/projects/libcxxabi/include" \
      -DLIBCXX_CXX_ABI_LIBRARY_PATH=${BUILDROOT}/llvm/build/lib && \
    ninja -j$(nproc) && \
    ninja install && \
    rm -rf ${BUILDROOT}/llvm && \
    echo "${llvm_ver}">${INSTPREFIX}/llvm.ver
