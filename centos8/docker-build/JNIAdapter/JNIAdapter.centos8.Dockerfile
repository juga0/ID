ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-libpepadapter:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

ARG JNIADAPTER_VERSION

## JNI Adapter
USER root
RUN yum -y install time java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
    yum clean all
USER pep-builder
COPY JNI_setup.sh /usr/bin/JNI_setup.sh
COPY JNI_build.sh /usr/bin/JNI_build.sh

RUN bash JNI_setup.sh && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    bash JNI_build.sh && \
    install -m 644 -t ${INSTPREFIX}/lib src/libpEpJNI.a && \
    install -m 755 -t ${INSTPREFIX}/lib src/libpEpJNI.so && \
    install -m 644 -t ${INSTPREFIX}/lib src/pEp.jar && \
    echo "${pepjni_ver}">${INSTPREFIX}/pEp_JNI.ver && \
    rm -rf ${BUILDROOT}/*
