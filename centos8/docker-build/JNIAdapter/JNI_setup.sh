#!/bin/bash
set -exuo pipefail

hg clone https://pep.foundation/dev/repos/pEpJNIAdapter ${BUILDROOT}/pEpJNIAdapter && \
cd ${BUILDROOT}/pEpJNIAdapter
hg up -Cr ${JNIADAPTER_VERSION}
hg purge -X local.conf --all --config extensions.purge=
cd ${BUILDROOT}/pEpJNIAdapter
export LC_ALL=en_US.UTF-8
export pepjni_ver=$(x=$(hg id -i) ; echo ${x/+/})
cd ${BUILDROOT}/pEpJNIAdapter/src
mkdir -p ${INSTPREFIX}/home/java
