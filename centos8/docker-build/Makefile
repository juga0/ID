SEQUOIA_VERSION= de497f59570437d448b293769eb57bf7a9741f30
ENGINE_VERSION= Release_2.1.0-RC21
LIBPEPADAPTER_VERSION= Release_2.1.0-RC25
JNIADAPTER_VERSION= Release_2.1.0-RC12
PYADAPTER_VERSION= Release_2.1.0-RC0
ETK_VERSION=889fa09  # master as of 2020-07-21
CURRENT_DISTRO= centos
CURRENT_DISTRO_VERSION= 8
PKG_DISTRO= RHEL
PKG_DISTRO_VERSION= 8.1
PKG_BUILD_IMAGE= ${DOCKER_REGISTRY_HOST}/fpm-$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)
PKG_TEST_IMAGE= ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-test
PKG_INSTALL_PATH= /opt/pEp/connector
PEP_MACHINE_DIR= $(PKG_INSTALL_PATH)/share/pEp
PKG_VERSION= 006
export

.PHONY: all help toolchain sequoia

all: fpm-pkg-builder jniadapter-RPM

help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""
	@echo "   1. make toolchain    - build the development toolchain"
	@echo "   2. make sequoia      - build Sequoia"
	@echo "   3. make sequoia-RPM  - build Sequoia RPM package"
	@echo "   3. make engine       - build pEpEngine"
	@echo "   3. make engine-RPM   - build pEpEngine RPM package"
	@echo "   4. make libpepadapter   - build libpEpAdapter"

fpm-pkg-builder:
	-docker pull $(PKG_BUILD_IMAGE):latest
	@docker build --build-arg CURRENT_DISTRO=$(CURRENT_DISTRO) \
		--build-arg CURRENT_DISTRO_VERSION=$(CURRENT_DISTRO_VERSION) \
		--cache-from $(PKG_BUILD_IMAGE):latest \
		--tag=$(PKG_BUILD_IMAGE):${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_BUILD_IMAGE):latest \
		-f pkg-builder/fpm-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}/Dockerfile \
		pkg-builder/fpm-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}
	@docker push $(PKG_BUILD_IMAGE):${CI_COMMIT_SHORT_SHA}
	@docker push $(PKG_BUILD_IMAGE):latest

toolchain:
	cd toolchain/ && make

sequoia:
	cd sequoia/ && make

engine:
	cd pEpEngine/ && make

libpepadapter:
	cd libpEpAdapter/ && make

pyadapter: 
	cd PyAdapter/ && make

etk-cli-connector:
	cd etk-cli/ && make ETK_PRODUCT_NAME=connector

etk-cli-plugin:
	cd etk-cli/ && make ETK_PRODUCT_NAME=plugin

etk-cli-predeploy:
	cd etk-cli/ && make ETK_PRODUCT_NAME=predeploy

jniadapter:
	cd JNIAdapter/ && make

pyadapter-RPM: fpm-pkg-builder
	-docker pull $(PKG_BUILD_IMAGE)-pyadapter:latest
	docker build --build-arg CURRENT_DISTRO=$(CURRENT_DISTRO) \
		--build-arg CURRENT_DISTRO_VERSION=$(CURRENT_DISTRO_VERSION) \
		--build-arg DOCKER_REGISTRY_HOST=${DOCKER_REGISTRY_HOST} \
		--build-arg PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
		--cache-from $(PKG_BUILD_IMAGE)-pyadapter:latest \
		--tag=$(PKG_BUILD_IMAGE)-pyadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_BUILD_IMAGE)-pyadapter:latest \
		pkg-builder/build-image/pEpPythonAdapter
	echo ${CI_COMMIT_SHORT_SHA}
	docker images
	docker push $(PKG_BUILD_IMAGE)-pyadapter:${CI_COMMIT_SHORT_SHA}
	docker push $(PKG_BUILD_IMAGE)-pyadapter:latest
	docker run -e ENGINE_VERSION=$(ENGINE_VERSION) \
		-e PYADAPTER_VERSION=$(PYADAPTER_VERSION) \
		-e PKG_NAME=pEpBundle -e TARGET_DISTRO=$(PKG_DISTRO) \
		-e TARGET_DISTRO_VERSION=$(PKG_DISTRO_VERSION) \
		-e PKG_VERSION=$(PKG_VERSION) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		-e PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
		--rm -v $(shell pwd)/pkg-builder/build-inside/pEpPythonAdapter:/build-inside:ro  \
		-v $(shell pwd)/pkg-builder/out:/out \
		-w /build-inside $(PKG_BUILD_IMAGE)-pyadapter:latest \
		/build-inside/build-package
	echo "Testing RPM"
	-docker pull $(PKG_TEST_IMAGE)-pyadapter:latest
	docker build --cache-from $(PKG_TEST_IMAGE)-pyadapter:latest \
		--tag=$(PKG_TEST_IMAGE)-pyadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_TEST_IMAGE)-pyadapter:latest pkg-builder/test-image/pEpPythonAdapter
	docker push $(PKG_TEST_IMAGE)-pyadapter:${CI_COMMIT_SHORT_SHA}
	docker push $(PKG_TEST_IMAGE)-pyadapter:latest
	docker run -e PKG_NAME=pEpBundle \
		-e TARGET_DISTRO=$(CURRENT_DISTRO) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		--rm -v $(shell pwd)/pkg-builder/test-inside:/test-inside:ro \
		-v $(shell pwd)/pkg-builder/out:/out:ro \
		-v $(shell pwd)/pkg-builder/test-logs:/test-logs \
		-w /test-inside $(PKG_TEST_IMAGE)-pyadapter /test-inside/pythonTest.sh || { echo "ERROR: the test phase failed." ; exit 1 ; }
	echo "Test of RPM & pEpPythonAdapter succeeded."


jniadapter-RPM: fpm-pkg-builder
	-docker pull $(PKG_BUILD_IMAGE)-jniadapter:latest
	@docker build --build-arg CURRENT_DISTRO=$(CURRENT_DISTRO) \
		--build-arg CURRENT_DISTRO_VERSION=$(CURRENT_DISTRO_VERSION) \
		--build-arg DOCKER_REGISTRY_HOST=${DOCKER_REGISTRY_HOST} \
		--build-arg PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
		--cache-from $(PKG_BUILD_IMAGE)-jniadapter:latest \
		--tag=$(PKG_BUILD_IMAGE)-jniadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_BUILD_IMAGE)-jniadapter:latest pkg-builder/build-image
	@docker push $(PKG_BUILD_IMAGE)-jniadapter:${CI_COMMIT_SHORT_SHA}
	@docker push $(PKG_BUILD_IMAGE)-jniadapter:latest
	@docker run -e ENGINE_VERSION=$(ENGINE_VERSION) \
		-e JNIADAPTER_VERSION=$(JNIADAPTER_VERSION) \
		-e PKG_NAME=pEpBundle -e TARGET_DISTRO=$(PKG_DISTRO) \
		-e TARGET_DISTRO_VERSION=$(PKG_DISTRO_VERSION) \
		-e PKG_VERSION=$(PKG_VERSION) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		-e PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
		--rm -v $(shell pwd)/pkg-builder/build-inside:/build-inside:ro  \
		-v $(shell pwd)/pkg-builder/out:/out \
		-w /build-inside $(PKG_BUILD_IMAGE)-jniadapter:latest /build-inside/build-package
	@echo "Testing RPM"
	-docker pull $(PKG_TEST_IMAGE):latest
	@docker build --cache-from $(PKG_TEST_IMAGE):latest \
		--tag=$(PKG_TEST_IMAGE):${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_TEST_IMAGE):latest pkg-builder/test-image
	@docker push $(PKG_TEST_IMAGE):${CI_COMMIT_SHORT_SHA}
	@docker push $(PKG_TEST_IMAGE):latest
	@docker run -e PKG_NAME=pEpBundle \
		-e TARGET_DISTRO=$(CURRENT_DISTRO) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		--rm -v $(shell pwd)/pkg-builder/test-inside:/test-inside:ro \
		-v $(shell pwd)/pkg-builder/out:/out:ro \
		-v $(shell pwd)/pkg-builder/test-logs:/test-logs \
		-w /test-inside $(PKG_TEST_IMAGE) /test-inside/test || { echo "ERROR: the test phase failed." ; exit 1 ; }
	@echo "Test of RPM & JNIAdapter succeeded."
	@docker build --tag=${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-pepjniadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-pepjniadapter:latest \
		-f pkg-builder/final-image/Dockerfile .
	@docker push ${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-pepjniadapter:latest

jni-and-pyadapter-RPM: fpm-pkg-builder
	-docker pull $(PKG_BUILD_IMAGE)-jni-and-pyadapter:latest
	@docker build --build-arg CURRENT_DISTRO=$(CURRENT_DISTRO) \
		--build-arg CURRENT_DISTRO_VERSION=$(CURRENT_DISTRO_VERSION) \
		--build-arg DOCKER_REGISTRY_HOST=${DOCKER_REGISTRY_HOST} \
		--build-arg PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
                --build-arg ETK_PRODUCT_NAME=$(ETK_PRODUCT_NAME) \
		--build-arg PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		--cache-from $(PKG_BUILD_IMAGE)-jni-and-pyadapter:latest \
		--tag=$(PKG_BUILD_IMAGE)-jni-and-pyadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_BUILD_IMAGE)-jni-and-pyadapter:latest \
		pkg-builder/build-image/combined
	@docker push $(PKG_BUILD_IMAGE)-jni-and-pyadapter:${CI_COMMIT_SHORT_SHA}
	@docker push $(PKG_BUILD_IMAGE)-jni-and-pyadapter:latest
	@docker run -e ENGINE_VERSION=$(ENGINE_VERSION) \
		-e JNIADAPTER_VERSION=$(JNIADAPTER_VERSION) \
		-e PYADAPTER_VERSION=$(PYADAPTER_VERSION) \
		-e ETK_VERSION=$(ETK_VERSION) \
		-e PKG_NAME=pEpBundle -e TARGET_DISTRO=$(PKG_DISTRO) \
		-e TARGET_DISTRO_VERSION=$(PKG_DISTRO_VERSION) \
		-e PKG_VERSION=$(PKG_VERSION) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		-e PEP_MACHINE_DIR=$(PEP_MACHINE_DIR) \
		--rm -v $(shell pwd)/pkg-builder/build-inside/combined:/build-inside:ro  \
		-v $(shell pwd)/pkg-builder/out:/out \
		-w /build-inside $(PKG_BUILD_IMAGE)-jni-and-pyadapter:latest \
		/build-inside/build-package
	@echo "Testing RPM"
	-docker pull $(PKG_TEST_IMAGE)-jni-and-pyadapter:latest
	@docker build --cache-from $(PKG_TEST_IMAGE)-jni-and-pyadapter:latest \
		--tag=$(PKG_TEST_IMAGE)-jni-and-pyadapter:${CI_COMMIT_SHORT_SHA} \
		--tag=$(PKG_TEST_IMAGE)-jni-and-pyadapter:latest \
		pkg-builder/test-image/combined
	@docker push $(PKG_TEST_IMAGE)-jni-and-pyadapter:${CI_COMMIT_SHORT_SHA}
	@docker push $(PKG_TEST_IMAGE)-jni-and-pyadapter:latest
	@docker run -e PKG_NAME=pEpBundle \
		-e TARGET_DISTRO=$(CURRENT_DISTRO) \
		-e PKG_INSTALL_PATH=$(PKG_INSTALL_PATH) \
		--rm -v $(shell pwd)/pkg-builder/test-inside:/test-inside:ro \
		-v $(shell pwd)/pkg-builder/out:/out:ro \
		-v $(shell pwd)/pkg-builder/test-logs:/test-logs \
		-w /test-inside $(PKG_TEST_IMAGE)-jni-and-pyadapter /test-inside/testCombined.sh || { echo "ERROR: the test phase failed." ; exit 1 ; }
	@echo "Test of RPM, JNIAdapter, and PythonAdpater succeeded."
	docker build --tag=${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-jni-and-pyadapter-run:${CI_COMMIT_SHORT_SHA} \
		--tag=${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-jni-and-pyadapter-run:latest \
		-f pkg-builder/final-image/combined/Dockerfile .
	docker push ${DOCKER_REGISTRY_HOST}/$(CURRENT_DISTRO)$(CURRENT_DISTRO_VERSION)-jni-and-pyadapter-run:latest
