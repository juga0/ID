ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-libpepadapter:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

ARG PYADAPTER_VERSION

## pEp Adapter
USER root
RUN sed -i 's/enabled=0/enabled=1/' /etc/yum.repos.d/CentOS-PowerTools.repo && \
    yum -y install python3-devel boost-python3-devel boost-locale && \
    yum clean all
USER pep-builder
RUN hg clone https://pep.foundation/dev/repos/pEpPythonAdapter ${BUILDROOT}/pEpPythonAdapter && \
    cd ${BUILDROOT}/pEpPythonAdapter && \
    hg up -Cr ${PYADAPTER_VERSION} && \
    export peppy_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    python3 setup.py build_ext --prefix=$INSTPREFIX && \
    mkdir -p $INSTPREFIX/lib64/python3.6/site-packages && \
    export PYTHONPATH=$INSTPREFIX/lib64/python3.6/site-packages/ && \
    python3 setup.py install --prefix=$INSTPREFIX --single-version-externally-managed && \
    echo "${peppy_ver}">${INSTPREFIX}/pEp_pyAdapter.ver && \
    rm -rf ${BUILDROOT}/*
