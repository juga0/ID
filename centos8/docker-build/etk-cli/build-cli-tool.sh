#!/bin/bash
set -exuo pipefail

GIT_SERVER=pep-security.lu/gitlab/
GIT_REPO=enterprise-editon/command-line-tool

product=${1:?Product name must be given}
product_version=${2:?Product version must be given}
if [ "${product}" = "predeploy" ] ; then
    product_tag_name=pEp_predeployment_tools
else
    product_tag_name=pEp_enterprise_tools_for_${product}
fi

mkdir -p ${BUILDROOT:?BUILDROOT not set}/command-line-tool
mkdir -p ${INSTPREFIX:?INSTPREFIX not set}
cd ${BUILDROOT}/command-line-tool

git clone https://gitlab-ci-token:$GITLAB_ACCESS_TOKEN@$GIT_SERVER$GIT_REPO .
git checkout ${ETK_VERSION}

# store version prior to anything else
git describe --tag --always > ${INSTPREFIX}/pEp_${product_tag_name}.ver

# HACK Allow building even if pEpPyhonAdapter is not installed
# while some module requiring pEp gets imported by setup.py.
echo "#" > pEp.py

make DESTDIR=${INSTPREFIX} ${product}

rm -rf ${BUILDROOT}/*
