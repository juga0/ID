ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-toolchain:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

ARG ETK_VERSION
ARG ETK_PRODUCT_NAME

ARG GITLAB_ACCESS_TOKEN

USER root
RUN sed -i 's/enabled=0/enabled=1/' /etc/yum.repos.d/CentOS-PowerTools.repo && \
    yum -y install python3-setuptools python3-pip && \
    yum clean all
USER pep-builder
COPY build-cli-tool.sh build-cli-tool.sh
RUN bash build-cli-tool.sh ${ETK_PRODUCT_NAME} ${ETK_VERSION}
