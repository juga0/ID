ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-engine:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

ARG LIBPEPADAPTER_VERSION

### libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ $BUILDROOT/libpEpAdapter && \
    cd $BUILDROOT/libpEpAdapter && \
    hg up -Cr ${LIBPEPADAPTER_VERSION} && \
    echo "ENGINE_LIB_PATH=${INSTPREFIX}/lib" >> local.conf && \
    echo "ENGINE_INC_PATH=${INSTPREFIX}/include" >> local.conf && \
    make && \
    make install PREFIX=$INSTPREFIX && \
    rm -rf ${BUILDROOT}/*
