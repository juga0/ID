#!/bin/bash -ex
# we should always set proper ownership before exiting, otherwise
# the created logs will have root:root ownership and we'll be unable
# to delete them from our host.
trap 'chown -R --reference /test-inside/test /test-logs/' EXIT

#must import public key
#rpm --import /tmp/public.asc

yum -y install /out/*.rpm

echo -e "Testing pEpPythonAdapter with no custom *PATHs set...\n"
{
python3 -c 'import pEp;'
} || true

echo -e "Testing pEpPythonAdapter with custom LD_LIBRARY_PATH & PYTHONPATH set...\n"
export PYTHONPATH=$PKG_INSTALL_PATH/lib/python3.6/site-packages
export LD_LIBRARY_PATH=$PKG_INSTALL_PATH/lib
python3 -c 'import pEp;'

echo -e "Testing pEpPythonAdapter Finished\n"
