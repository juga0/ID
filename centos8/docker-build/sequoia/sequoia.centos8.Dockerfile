ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-toolchain:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

ARG SEQUOIA_VERSION

### Sequoia
RUN curl -O https://gitlab.com/sequoia-pgp/sequoia/-/archive/${SEQUOIA_VERSION}/sequoia-${SEQUOIA_VERSION}.tar.bz2 && \
    tar -xf sequoia-${SEQUOIA_VERSION}.tar.bz2 && \
    cd sequoia-${SEQUOIA_VERSION} && \
    make build-release PYTHON=disable PREFIX=$INSTPREFIX && \
    make install PYTHON=disable PREFIX=$INSTPREFIX && \
    echo "${SEQUOIA_VERSION}"|cut -c-7>${INSTPREFIX}/sequoia.ver && \
    rm -rf ${BUILDROOT}/*
