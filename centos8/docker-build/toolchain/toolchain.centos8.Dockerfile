ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${CURRENT_DISTRO}:${CURRENT_DISTRO_VERSION}

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

  # Create basic dirs
RUN mkdir -p ${BUILDROOT}
RUN mkdir -p ${INSTPREFIX}
RUN mkdir -p ${OUTDIR}

  # Setup Env
ENV PATH = ${PATH}:${DIR_YML2}

WORKDIR /build

RUN yum -y install git clang rustc cargo make pkg-config \ 
                   nettle-devel openssl-devel glibc-langpack-en \
	           libxml2-devel gmp gmp-devel sqlite-devel \
                   mercurial libuuid libuuid-devel automake \
		   libtool tcl which bzip2 epel-release && \
                   yum clean all

RUN yum -y install patchelf  capnproto && \
                   yum clean all

RUN adduser -U -r -s /bin/sh pep-builder && \
    mkdir -p /home/pep-builder

RUN chown -R pep-builder:pep-builder ${BUILDROOT} && \
    chown -R pep-builder:pep-builder ${INSTPREFIX} && \
    chown -R pep-builder:pep-builder ${OUTDIR} && \
    chown -R pep-builder:pep-builder /home/pep-builder

USER pep-builder
