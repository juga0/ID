ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-engine:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_MACHINE_DIR /usr/share/pEp

### libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ $BUILDROOT/libpEpAdapter && \
    cd $BUILDROOT/libpEpAdapter && \
    echo "ENGINE_LIB_PATH=${INSTPREFIX}/lib" >> local.conf && \
    echo "ENGINE_INC_PATH=${INSTPREFIX}/include" >> local.conf && \
#    source scl_source enable devtoolset-8 && \
#    export LC_ALL=en_US.UTF-8 && \
#   # export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
#    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
#    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
#    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${BUILDROOT}/usr/lib64 && \
#    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
  make && \
  make install PREFIX=$INSTPREFIX

RUN rm -rf ${BUILDROOT}/* && \
    rm -rf /var/lib/apt/lists/*
