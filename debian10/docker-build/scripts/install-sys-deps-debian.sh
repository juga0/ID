#!/bin/bash

# Installs the Common System Dependencies for building the pEp Software Stack.
#
# REQUIRED:
# * Debian 10 minimal install
#
# TODO:
# *
# * Error handling
# * Port this script to:
#     * RHEL 6,7,8
#     * Fedora
#     * MacOS
#
#
echo 'Run As root: usermod -a -G sudo <username>'

set -exuo pipefail
apt-get update

# Common System Dependencies
apt-get install -y curl openssl libssl-dev pkg-config git rustc cargo mercurial capnproto clang sqlite3 libsqlite3-0 libsqlite3-dev python3 python3-lxml build-essential automake libtool autoconf make nettle-dev capnproto uuid-dev

## uncomment for: pEpPythonAdapter system dependencies
#apt-get install -y python3-setuptools libboost-python-dev libboost-locale1.67-dev
#
## uncomment for: pEpJSONServerAdapter dependencies
#apt-get install -y build-essential libboost1.67-dev libboost-system1.67-dev libboost-filesystem1.67-dev libboost-program-options1.67-dev libboost-thread1.67-dev libgpgme-dev uuid-dev googletest libevent-dev libevhtp-dev

rm -rf /var/lib/apt/lists/*
