ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-toolchain:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out


### Sequoia
RUN git clone https://gitlab.com/sequoia-pgp/sequoia.git -b pep-engine && \
    cd sequoia && \
    make build-release PYTHON=disable && \
    make install PYTHON=disable PREFIX=$INSTPREFIX
