ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${CURRENT_DISTRO}:${CURRENT_DISTRO_VERSION}

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out

  # Create basic dirs
RUN mkdir -p ${BUILDROOT}
RUN mkdir -p ${INSTPREFIX}
RUN mkdir -p ${OUTDIR}

  # Setup Env
ENV PATH = ${PATH}:${DIR_YML2}

WORKDIR /build

COPY ./scripts/install-sys-deps-debian.sh /usr/local/bin/install-sys-deps-debian.sh

RUN /usr/local/bin/install-sys-deps-debian.sh

RUN adduser --shell /bin/sh --disabled-password --gecos "" pep-builder

RUN chown -R pep-builder:pep-builder ${BUILDROOT} && \
    chown -R pep-builder:pep-builder ${INSTPREFIX} && \
    chown -R pep-builder:pep-builder ${OUTDIR} && \
    chown -R pep-builder:pep-builder /home/pep-builder

USER pep-builder
