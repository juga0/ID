#!/bin/bash

# Installs the Common System Dependencies for building the pEp Software Stack.
#
# REQUIRED:
# * Debian 10 minimal install
#
# TODO:
# *
# * Error handling
# * Port this script to:
#     * RHEL 6,7,8
#     * Fedora
#     * MacOS
#
#
echo 'Run As root: usermod -a -G sudo <username>'

set -x
sudo apt-get update

# Common System Dependencies
sudo apt-get install -y sudo curl openssl libssl-dev pkg-config git rustc cargo mercurial capnproto clang sqlite3 libsqlite3-0 libsqlite3-dev python3 python3-lxml build-essential automake libtool autoconf make nettle-dev capnproto uuid-dev

# pEpPythonAdapter system dependencies
sudo apt-get install -y python3-setuptools libboost-python-dev libboost-locale1.67-dev

# pEpJNIAdapter system dependencies
sudo apt-get install -y default-jdk
