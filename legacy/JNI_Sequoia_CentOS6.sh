# FYI: VM MAC 080027DC5DB6

# CentOS 6.7 from
# http://mirror.nsc.liu.se/centos-store/6.7/isos/x86_64/CentOS-6.7-x86_64-minimal.iso
# via
# http://vault.centos.org/6.7/isos/x86_64/
# ( does update to 6.10 )

export BUILDDIR=/root/build
export INSTALLDIR=/root/opt
export WORKDIR=/root/work
mkdir -p "${WORKDIR}" "${BUILDDIR}" "${INSTALLDIR}"

chcon -vR unconfined_u:object_r:ssh_home_t:s0 ${WORKDIR}/.ssh/

yum -y install screen
# yum -y install redhat-lsb-core

yum -y install wget
wget -O ${WORKDIR}/rustup https://sh.rustup.rs
chmod +x ${WORKDIR}/rustup

yum -y install patch git cmake yum-utils rpm-build rpmdevtools
yum -y install python27 rh-python35
yum -y install openssl-devel zlib-devel libxml2-devel gmp gmp-devel sqlite-devel

yum -y install centos-release-scl
# ^ on RHEL: yum-config-manager --enable rhel-server-rhscl-7-rpms

yum -y install devtoolset-8

yum -y install devtoolset-8-gcc devtoolset-8-gcc-c++

yum -y install python27 rh-python35
yum -y install python-lxml

yum -y install mercurial

# The wrong one: yum -y install uuid-devel libuuid-devel
yum -y install libuuid libuuid-devel

yum -y install automake libtool

# SQLite needs TCL to build :-/
yum -y install tcl

# yum -y install java-1.7.0-openjdk java-1.7.0-openjdk-devel
yum -y install java-1.7.0-openjdk java-1.7.0-openjdk-devel

## RHEL/CentOS 6 32-Bit ##
# wget http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
# rpm -ivh epel-release-6-8.noarch.rpm

## RHEL/CentOS 6 64-Bit ##
wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh epel-release-6-8.noarch.rpm

yum -y install patchelf





hg clone https://pep.foundation/dev/repos/yml2 ${WORKDIR}/yml2

hg clone https://pep.foundation/dev/repos/pEpEngine ${WORKDIR}/pEpEngine
# write local.conf

hg clone https://pep.foundation/dev/repos/pEpJNIAdapter



alias wget='wget -nc'


# ============================================
# Deps of LLVM/clang to compile with GCC
# ============================================






export cmake_ver=3.14.3
export cmake_ver=3.14.5
cd ${WORKDIR}
wget https://github.com/Kitware/CMake/releases/download/v${cmake_ver}/cmake-${cmake_ver}.tar.gz \
    -O ${WORKDIR}/cmake-${cmake_ver}.tar.gz
tar -C ${WORKDIR} -xf ${WORKDIR}/cmake-${cmake_ver}.tar.gz
cd ${WORKDIR}/cmake-${cmake_ver}
scl enable devtoolset-8 'bash -vxe' <<EOF
MAKEFLAGS=-j5 ./bootstrap --prefix=${INSTALLDIR}/cmake -- -DCMAKE_BUILD_TYPE:STRING=Release
make -j5
make install
echo \${cmake_ver}>${INSTALLDIR}/cmake.ver
EOF


export ninja_ver=1.9.0
cd ${WORKDIR}
wget https://github.com/ninja-build/ninja/archive/v${ninja_ver}.tar.gz \
    -O ninja-${ninja_ver}.tar.gz
tar -C ${WORKDIR} -xf ninja-${ninja_ver}.tar.gz
#
scl enable devtoolset-8 'bash -vxe' <<EOF
cd ${WORKDIR}/ninja-${ninja_ver}
python configure.py --verbose --bootstrap
mkdir -p ${INSTALLDIR}/ninja/bin
install -m 755 ninja ${INSTALLDIR}/ninja/bin/ninja
echo \${ninja_ver}>${INSTALLDIR}/ninja.ver
EOF



# Credits go to many, and in particular:
#   https://shaharmike.com/cpp/build-clang/

export llvm_ver=8.0.0
cd ${WORKDIR}
wget https://releases.llvm.org/${llvm_ver}/llvm-${llvm_ver}.src.tar.xz
wget https://releases.llvm.org/${llvm_ver}/libcxx-${llvm_ver}.src.tar.xz
wget https://releases.llvm.org/${llvm_ver}/libcxxabi-${llvm_ver}.src.tar.xz
wget https://releases.llvm.org/${llvm_ver}/lld-${llvm_ver}.src.tar.xz
wget https://releases.llvm.org/${llvm_ver}/lldb-${llvm_ver}.src.tar.xz
# extra:tools/clang/tools/extra compiler-rt:projects/compiler-rt projects/libcxx
mkdir -p llvm
tar -C llvm -xf ~/llvm-${llvm_ver}.src.tar.xz
for COMP in cfe:tools/clang libcxx:projects/libcxx libcxxabi:projects/libcxxabi lld:tools/lld ; do  # also add to -DLLVM_ENABLE_PROJECTS= below
    mkdir -p llvm/llvm-${llvm_ver}.src/${COMP#*:}
    tar -C llvm/llvm-${llvm_ver}.src/${COMP#*:} -xf ~/${COMP%:*}-${llvm_ver}.src.tar.xz --strip-components=1
done
#
mkdir -p opt/llvm
scl enable python27 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/cmake/bin:${INSTALLDIR}/ninja/bin:\$PATH
CC=\$(which gcc)
CXX=\$(which g++)
PYEXE=\$(which python)
mkdir -p ${WORKDIR}/llvm/build/
cd ${WORKDIR}/llvm/build/
cmake_args=( 
  -GNinja
  "-DCMAKE_CXX_COMPILER=\$CXX" "-DCMAKE_C_COMPILER=\$CC" "-DPYTHON_EXECUTABLE=\$PYEXE"
  -DLLVM_TARGETS_TO_BUILD=X86
  -DLLVM_INCLUDE_EXAMPLES=OFF -DLLVM_INCLUDE_DOCS=OFF
  -DLIBCXX_ENABLE_SHARED=YES -DLIBCXX_ENABLE_STATIC=NO
  -DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=NO
  -DLIBCXX_CXX_ABI=libcxxabi
  -DCMAKE_BUILD_TYPE=Release
  -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}/llvm
  -DLIBCXX_CXX_ABI=libcxxabi
  -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${WORKDIR}/llvm/build/include/c++/v1;${WORKDIR}/llvm/llvm-\${llvm_ver}.src/projects/libcxxabi/include"
  -DLIBCXX_CXX_ABI_LIBRARY_PATH=${WORKDIR}/llvm/build/lib    )
# find . -iwholename '*cmake*' -not -name CMakeLists.txt  # -delete
cmake "\${cmake_args[@]}" ../llvm-${llvm_ver}.src
ninja
ninja install
echo \${llvm_ver}>${INSTALLDIR}/llvm.ver
EOF



# ============================================
# Things to compile with clang
# ============================================

# To work around rustup requirements, get a modern version of curl first

export curl_ver=7.64.1
export curl_ver=7.65.1
cd ${WORKDIR}
wget "https://curl.haxx.se/download/curl-${curl_ver}.tar.gz" \
    -O ${WORKDIR}/curl-${curl_ver}.tar.gz
tar -C ${WORKDIR} -xf ${WORKDIR}/curl-${curl_ver}.tar.gz
mkdir -p ${INSTALLDIR}/curl
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:\$PATH
cd ${WORKDIR}/curl-${curl_ver}
./configure --prefix=${INSTALLDIR}/curl/ --with-ssl --with-zlib --without-nghttp2 \
    --disable-ftp --enable-file --without-rtsp \
    CC=clang CXX=clang++ "LDFLAGS=-Wl,-rpath -Wl,${INSTALLDIR}/curl/lib"
make -j5
# make test
make install
echo \${curl_ver}>${INSTALLDIR}/curl.ver
EOF



# Auto-install rust
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/curl/bin:${INSTALLDIR}/llvm/bin:\$PATH
## export RUSTUP_UPDATE_ROOT=file://${WORKDIR}/rustup.dl
# mkdir -p ${WORKDIR}/rustup.dl/dist/x86_64-unknown-linux-gnu/rustup-init"
export CC=\$(which gcc)
export CXX=\$(which g++)
_arch=x86_64-unknown-linux-gnu
_rustup_init=rustup.dl/dist/\${_arch}/rustup-init
if [ -x "\$_rustup_init" ] ; then
  "\$_rustup_init" -y -v --no-modify-path --default-host x86_64-unknown-linux-gnu
else
  if [ ! -x ${WORKDIR}/rustup ] ; then
    wget https://sh.rustup.rs -O ${WORKDIR}/rustup
  fi
  # bash -vx ${WORKDIR}/rustup -y -v --no-modify-path
  bash -vx ${WORKDIR}/rustup -y
fi
EOF


# ... or upgrade rust
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/curl/bin:${INSTALLDIR}/llvm/bin:\$PATH
## export RUSTUP_UPDATE_ROOT=file://${WORKDIR}/rustup.dl
# mkdir -p ${WORKDIR}/rustup.dl/dist/x86_64-unknown-linux-gnu/rustup-init"
export CC=\$(which gcc)
export CXX=\$(which g++)

bash -vx ${WORKDIR}/rustup -y --no-modify-path
EOF



# Install Sequoia dependency "capnproto" from source
#  -- following to https://capnproto.org/install.html, slightly adapted
export capnp_ver=0.7.0
cd ${WORKDIR}
rm -rf  ${INSTALLDIR}/capnp
mkdir -p ${INSTALLDIR}/capnp
wget https://capnproto.org/capnproto-c++-${capnp_ver}.tar.gz \
    -O ${WORKDIR}/capnproto-c++-${capnp_ver}.tar.gz
# rm -rf ${WORKDIR}/capnproto-c++-${capnp_ver}
tar -C ${WORKDIR} -xf ${WORKDIR}/capnproto-c++-${capnp_ver}.tar.gz
cd ${WORKDIR}/capnproto-c++-${capnp_ver}
#
if ! grep -q aligned_alloc src/kj/table.c++ ; then patch -p1 <<EOF
diff --git a/src/kj/table.c++ b/src/kj/table.c++
index 5f363e0..772adac 100644
--- a/src/kj/table.c++
+++ b/src/kj/table.c++
@@ -22,6 +22,7 @@
 #include "table.h"
 #include "debug.h"
 #include <stdlib.h>
+#include <malloc.h>

 namespace kj {
 namespace _ {
@@ -307,7 +308,7 @@ void BTreeImpl::growTree(uint minCapacity) {
 #else
   // Let's use the C11 standard.
   NodeUnion* newTree = reinterpret_cast<NodeUnion*>(
-      aligned_alloc(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
+      memalign(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
   KJ_ASSERT(newTree != nullptr, "memory allocation failed", newCapacity);
 #endif
EOF
fi

# if ! grep -q aligned_alloc src/kj/table.c++ ; then patch -p1 <<EOF
# diff --git a/src/kj/table.c++ b/src/kj/table.c++
# index 5f363e0..772adac 100644
# --- a/src/kj/table.c++
# +++ b/src/kj/table.c++
# @@ -307,7 +308,7 @@ void BTreeImpl::growTree(uint minCapacity) {
#  #else
#    // Let's use the C11 standard.
#    NodeUnion* newTree = reinterpret_cast<NodeUnion*>(
# -      aligned_alloc(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
# +      posix_memalign(sizeof(BTreeImpl::NodeUnion), newCapacity * sizeof(BTreeImpl::NodeUnion)));
#    KJ_ASSERT(newTree != nullptr, "memory allocation failed", newCapacity);
#  #endif
# EOF
# fi

#
# ATTENTION ATTENTION ATTENTION
# =============================
#
# The line src/kj/filesystem-disk-test.c++ (tests fail)
#
# But, also that we disable sequoia-store in sequoia.
#
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/curl/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/capnproto-c++-\${capnp_ver}
./configure --prefix=${INSTALLDIR}/capnp \
    CC=clang \
    CXX=clang++ "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++ " \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/capnp/lib -fuse-ld=lld"
make -j5 all
: > src/kj/filesystem-disk-test.c++
make -j5 check
make install
echo \${capnp_ver}>${INSTALLDIR}/capnp.ver
EOF



#
# QA:
#  * Need LD_RUN_PATH and LD_LIBRARY_PATH to lib-path of LLVM (or configure steps will fail)
#  * Need CC=clang and CXX=clang++
#  * CXXFLAGS needs: -I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++
#  * Repeat -stdlib=libc++ in both CXXFLAGS and "CXX=clang++ -stdlib=libc++"
#  * Add to LDFLAGS: -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib
#  * Add any other local lib to LDFLAGS: -L<lib-path> -Wl,-rpath,<lib-path>
#  * Append to LDFLAGS: -Wl,-rpath,<lib-prefix>
#  * Append to LDFLAGS: -fuse-ld=lld
#  * Add local lib's pkgconfig directories to PKG_CONFIG_PATH
#
# Some docu for C++: https://libcxx.llvm.org/docs/UsingLibcxx.html
# https://wiki.musl-libc.org/building-llvm.html
# 



export gmp_ver=6.1.2
cd ${WORKDIR}
wget https://gmplib.org/download/gmp/gmp-${gmp_ver}.tar.bz2 \
    -O ${WORKDIR}/gmp-${gmp_ver}.tar.bz2
# rm -rf ${WORKDIR}/gmp-${gmp_ver}
tar -C ${WORKDIR} -xf ${WORKDIR}/gmp-${gmp_ver}.tar.bz2
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/gmp-${gmp_ver}
./configure --prefix=${INSTALLDIR}/gmp \
    --enable-cxx \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/gmp/lib -fuse-ld=lld"
make -j5
make install
echo \${gmp_ver}>${INSTALLDIR}/gmp.ver
EOF


 
export nettle_ver=3.4
export nettle_ver=3.5
cd ${WORKDIR}
wget https://ftp.gnu.org/gnu/nettle/nettle-${nettle_ver}.tar.gz \
    -O ${WORKDIR}/nettle-${nettle_ver}.tar.gz
# rm -rf ${WORKDIR}/nettle-${nettle_ver}
tar -C ${WORKDIR} -xf ${WORKDIR}/nettle-${nettle_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/curl/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/nettle-${nettle_ver}
./configure --prefix=${INSTALLDIR}/nettle \
    --enable-x86-aesni \
    CC=clang CFLAGS="-I${INSTALLDIR}/gmp/include" \
    CXX=please-fail \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/gmp/lib -Wl,-rpath,${INSTALLDIR}/gmp/lib -Wl,-rpath,${INSTALLDIR}/nettle/lib64 -fuse-ld=lld" 
make -j5
make install
echo \${nettle_ver}>${INSTALLDIR}/nettle.ver
EOF




cd ${WORKDIR}
git clone git@gitlab.com:sequoia-pgp/sequoia.git sequoia

cd ${WORKDIR}/sequoia
# git pull
git reset --hard master
git clean -f -x
git checkout master
git merge origin/master
export sq_changeset=$(git describe)
export sq_src=${WORKDIR}/sequoia


cd ${WORKDIR}/sequoia
export sq_changeset=$(git describe)
export sq_src=${WORKDIR}/sequoia



# sq_changeset=master
export sq_changeset=1d35557a8f7866141f84b2764287f384a8e12872  # vb
export sq_src=${WORKDIR}/sequoia-${sq_changeset}
cd ${WORKDIR}
# sq_changeset=36f757acfe2dcef3352527c2850379771b16bee4  # Release 0.6.0
# sq_changeset=ba9363f
wget https://gitlab.com/sequoia-pgp/sequoia/-/archive/${sq_changeset}/sequoia-${sq_changeset}.tar.bz2 \
  -O ${WORKDIR}/sequoia-${sq_changeset}.tar.gz
# rm -rf ${INSTALLDIR}/sequoia ${WORKDIR}/sequoia-${sq_changeset}
tar -C ${WORKDIR} -xf ${WORKDIR}/sequoia-${sq_changeset}.tar.gz
mkdir -p ${INSTALLDIR}/sequoia
cd "${sq_src}"






# patch --dry-run -p1 <${WORKDIR}/macos-sequoia.patch
scl enable rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/cmake/bin:${INSTALLDIR}/curl/bin:${INSTALLDIR}/nettle/bin:${INSTALLDIR}/capnp/bin:${INSTALLDIR}/llvm/bin:\$HOME/.cargo/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/rh-python35${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/nettle/lib64/pkgconfig
# :/usr/lib64/pkgconfig:/usr/share/pkgconfig
cd "${sq_src}"
pkg-config --libs-only-L nettle hogweed  # if hogweed fails, we miss a valid libgmp
_Wl_rpath=\$(pkg-config --libs-only-L nettle hogweed)
_Wl_rpath=\${_Wl_rpath//-L\//-Wl,-rpath,\/}


# cargo clean -p sequoia-ffi -p sequoia-openpgp-ffi -p sequoia-ffi-macros
# export LD_RUN_PATH="${INSTALLDIR}/gmp/lib"
# export LD_LIBRARY_PATH="${INSTALLDIR}/gmp/lib:${INSTALLDIR}/nettle/lib64:/opt/rh/rh-python35${WORKDIR}/usr/lib64:${INSTALLDIR}/sequoia/usr/lib"
# - must add LDFLAGS and CPPFLAGS to make openpgp-ffi/examples compile :-/
# - note: "install" calls "build-release"

export CC=clang
export CFLAGS="-I${INSTALLDIR}/gmp/include \$(pkg-config --cflags nettle hogweed) -I${INSTALLDIR}/pep/include"
export CXX="clang++ -stdlib=libc++"
export CXXFLAGS="-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++ -I${INSTALLDIR}/gmp/include \$(pkg-config --cflags nettle hogweed) -I${INSTALLDIR}/pep/include"
# export CXXFLAGS="-I${INSTALLDIR}/llvm/include/c++/v1"

export LDFLAGS="-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/gmp/lib -Wl,-rpath,${INSTALLDIR}/gmp/lib \$(pkg-config --libs-only-L nettle hogweed) \${_Wl_rpath} -L${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -L"${sq_src}"/target/release -Wl,-rpath,${INSTALLDIR}/sequoia/lib -fuse-ld=lld"

_rust_L="-Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/gmp/lib \${_Wl_rpath} -Wl,-rpath,${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/sequoia/lib"
_rust_L=\${_rust_L//-Wl,/-C link-arg=-Wl,}
export RUSTFLAGS=" \${_rust_L} -L${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/gmp/lib \$(pkg-config --libs-only-L nettle hogweed) -L${INSTALLDIR}/pep/lib"

export RUSTDOCFLAGS="\$RUSTFLAGS"

export CARGO_INCREMENTAL=true

# cargo clean -p lalrpop
# cargo clean -p sequoia_rfc2822
# cargo clean

grep -q 'lto = true' Cargo.toml || patch -p1 <<PATCH
--- a/Cargo.toml       2019-04-30 16:22:29.920033773 +0200
+++ b/Cargo.toml       2019-04-30 16:22:52.344996082 +0200
@@ -40,4 +40,6 @@
 path = "src/lib.rs"

 [profile.release]
-debug = true
+debug = false
+lto = true
+panic = 'abort'
PATCH

# Must call build-release before install!
if false ; then
  :
elif true ; then
  # ---------------------------------------------
# breaks tests: export LD_LIBRARY_PATH="\$LD_LIBRARY_PATH:${INSTALLDIR}/llvm/lib:${INSTALLDIR}/gmp/lib:/root/opt/nettle/lib64:${INSTALLDIR}/pep/lib"
set +vx
echo PATH=\$PATH
echo LD_RUN_PATH=\$LD_RUN_PATH
echo LD_LIBRARY_PATH=\$LD_LIBRARY_PATH
echo PKG_CONFIG_PATH=\$PKG_CONFIG_PATH
echo CC=\$CC
echo CFLAGS=\$CFLAGS
echo CXX=\$CXX
echo CXXFLAGS=\$CXXFLAGS
echo LDFLAGS=\$LDFLAGS
echo RUSTFLAGS=\$RUSTFLAGS
echo RUSTDOCFLAGS=\$RUSTDOCFLAGS
echo CARGO_FLAGS=\$CARGO_FLAGS
#
set -vx
make -C "${sq_src}" DESTDIR=${INSTALLDIR}/sequoia PREFIX=/ \
    "CARGO_FLAGS=--verbose" \
    PYTHON=disable build-release
make -C "${sq_src}" DESTDIR=${INSTALLDIR}/sequoia PREFIX=/ \
    "CARGO_FLAGS=--verbose" \
    PYTHON=disable install
RUST_BACKTRACE=full make -C "${sq_src}" DESTDIR=${INSTALLDIR}/sequoia PREFIX=/ \
    "CARGO_FLAGS=--verbose" \
    PYTHON=disable check
else
  # ---------------------------------------------
  # Only compile (and test) the necessary library

rm -f ${INSTALLDIR}/sequoia/bin/sq ${INSTALLDIR}/sequoia/bin/sqv

export PREFIX=/
export DESTDIR=${INSTALLDIR}/sequoia
export CARGO_FLAGS=--verbose
export CARGO_TARGET_DIR="${sq_src}"/target
export CARGO_TEST_ARGS=--all

# cargo build \${CARGO_FLAGS} --release --all
cargo build \${CARGO_FLAGS} --release -p sequoia-openpgp-ffi
make -Copenpgp-ffi build-release
cargo test --release -p sequoia-openpgp-ffi
#
install -d ${INSTALLDIR}/sequoia/lib/sequoia
# install -t ${INSTALLDIR}/sequoia/lib/sequoia \
#     \${CARGO_TARGET_DIR}/release/sequoia-public-key-store
install -d ${INSTALLDIR}/sequoia/bin
# install -t ${INSTALLDIR}/sequoia/bin \
#     \${(}CARGO_TARGET_DIR}/release/sq
make -Copenpgp-ffi install
# make -Cffi install
# make -Csqv install

fi  # ---------------------------------------------

mkdir -p ${INSTALLDIR}/sequoia/include/sequoia/openpgp
# install openpgp-ffi/include/sequoia/openpgp/error.h  ${INSTALLDIR}/sequoia/include/sequoia/openpgp/error.h
# install openpgp-ffi/include/sequoia/openpgp/crypto.h ${INSTALLDIR}/sequoia/include/sequoia/openpgp/crypto.h
# install openpgp-ffi/include/sequoia/openpgp/types.h  ${INSTALLDIR}/sequoia/include/sequoia/openpgp/types.h
install openpgp-ffi/include/sequoia/openpgp/*.h  ${INSTALLDIR}/sequoia/include/sequoia/openpgp

sed -i 's#^prefix=.*\$#prefix=${INSTALLDIR}/sequoia#' ${INSTALLDIR}/sequoia/share/pkgconfig/sequoia-openpgp.pc  || true
sed -i 's#^prefix=.*\$#prefix=${INSTALLDIR}/sequoia#' ${INSTALLDIR}/sequoia/share/pkgconfig/sequoia.pc  || true

echo \${sq_changeset}>${INSTALLDIR}/sq.ver
EOF





# ==============================



export m4_ver=1.4.18
cd ${WORKDIR}
wget https://ftp.gnu.org/gnu/m4/m4-${m4_ver}.tar.gz
    -O ${WORKDIR}/m4-${m4_ver}.tar.gz
tar -C ${WORKDIR} -xf m4-${m4_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/m4-${m4_ver}
./configure --prefix=${INSTALLDIR}/autotools
make -j5
make install
echo \${m4_ver}>${INSTALLDIR}/m4.ver
EOF



export libtool_ver=2.4.6
cd ${WORKDIR}
wget https://ftp.gnu.org/gnu/libtool/libtool-${libtool_ver}.tar.gz \
    -O ${WORKDIR}/libtool-${libtool_ver}.tar.gz
tar -C ${WORKDIR} -xf libtool-${libtool_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/libtool-${libtool_ver}
./configure --with-sysroot=${INSTALLDIR}/sysroot --prefix=${INSTALLDIR}/autotools
make -j5
make install
echo \${libtool_ver}>${INSTALLDIR}/libtool.ver
EOF





export autoconf_ver=2.69
cd ${WORKDIR}
wget https://ftp.gnu.org/gnu/autoconf/autoconf-${autoconf_ver}.tar.gz \
    -O ${WORKDIR}/autoconf-${autoconf_ver}.tar.gz
tar -C ${WORKDIR} -xf autoconf-${autoconf_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:${INSTALLDIR}/autotools/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/autoconf-${autoconf_ver}
./configure --prefix=${INSTALLDIR}/autotools
make -j5
make install
echo \${autoconf_ver}>${INSTALLDIR}/autoconf.ver
EOF


export automake_ver=1.16.1
cd ${WORKDIR}
wget https://ftp.gnu.org/gnu/automake/automake-${automake_ver}.tar.gz \
    -O ${WORKDIR}/automake-${automake_ver}.tar.gz
tar -C ${WORKDIR} -xf automake-${automake_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:${INSTALLDIR}/autotools/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/automake-${automake_ver}
./configure --prefix=${INSTALLDIR}/autotools
make -j5
make install
echo \${automake_ver}>${INSTALLDIR}/automake.ver
EOF


export pkg_config_ver=0.29.2
cd ${WORKDIR}
wget https://pkg-config.freedesktop.org/releases/pkg-config-${pkg_config_ver}.tar.gz \
    -O ${WORKDIR}/pkg-config-${pkg_config_ver}.tar.gz
tar -C ${WORKDIR} -xf pkg-config-${pkg_config_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:${INSTALLDIR}/autotools/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/pkg-config-${pkg_config_ver}
./configure  --with-internal-glib --with-sysroot=${INSTALLDIR}/sysroot --prefix=${INSTALLDIR}/autotools
make -j5
make install
echo \${pkg_config_ver}>${INSTALLDIR}/pkg_config.ver
EOF







export asn1c_ver=0.9.28
rm -rf ${WORKDIR}/asn1c-${asn1c_ver}
# wget https://github.com/vlm/asn1c/archive/v${asn1c_ver}.tar.gz
# export asn1c_ver=master
cd ${WORKDIR}
wget https://github.com/vlm/asn1c/archive/v${asn1c_ver}.tar.gz \
    -O ${WORKDIR}/asn1c-${asn1c_ver}.tar.gz
# mkdir -p ${WORKDIR}/asn1c-${asn1c_ver}
# tar -C ${WORKDIR}/asn1c-${asn1c_ver} --strip-components=1 -xf ${WORKDIR}/asn1c-${asn1c_ver}.tar.gz
tar -C ${WORKDIR} -xf ${WORKDIR}/asn1c-${asn1c_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:${INSTALLDIR}/autotools/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:${INSTALLDIR}/autotools/lib
cd ${WORKDIR}/asn1c-${asn1c_ver}
test -f configure || autoreconf -iv
./configure --prefix=${INSTALLDIR}/asn1c \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -fuse-ld=lld" 
make -j5
make install
echo \${asn1c_ver}>${INSTALLDIR}/asn1c.ver
EOF




export libiconv_ver=1.16
cd ${WORKDIR}
wget https://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_ver}.tar.gz \
    -O ${WORKDIR}/libiconv-${libiconv_ver}.tar.gz
# mkdir -p ${WORKDIR}/libiconv-${libiconv_ver}
# rm -rf ${WORKDIR}/libiconv-${libiconv_ver}
tar -C ${WORKDIR} -xf ${WORKDIR}/libiconv-${libiconv_ver}.tar.gz
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/libiconv-\${libiconv_ver}
./configure --prefix=${INSTALLDIR}/libiconv \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -fuse-ld=lld" 
make -j5
make install
echo \${libiconv_ver}>${INSTALLDIR}/libiconv.ver
EOF

# -I${INSTALLDIR}/llvm/include/c++/v1 



export libetpan_ver=af2aad552dd0ecc6a943f78e5c77dc03ebbc0028
cd ${WORKDIR}
wget https://github.com/fdik/libetpan/archive/${libetpan_ver}.tar.gz \
    -O ${WORKDIR}/libetpan-${libetpan_ver}.tar.gz
tar -C ${WORKDIR} -xf ${WORKDIR}/libetpan-${libetpan_ver}.tar.gz
mkdir -p ${INSTALLDIR}/libetpan
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/libetpan-${libetpan_ver}
test -f configure || NOCONFIGURE=absolutely ./autogen.sh
./configure --prefix=${INSTALLDIR}/libetpan \
    --without-openssl --without-gnutls --without-sasl \
    --without-curl --without-expat --without-zlib \
    --disable-dependency-tracking \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -I${INSTALLDIR}/libiconv/include -stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -fuse-ld=lld" 
make -j5
make install
echo \${libetpan_ver}>${INSTALLDIR}/libetpan.ver
EOF

# Does it link iconv ???




export sqlite3_ver=3280000
export sqlite3_ver=3290000
cd ${WORKDIR}
wget https://sqlite.org/2019/sqlite-src-${sqlite3_ver}.zip \
    -O ${WORKDIR}/sqlite-src-${sqlite3_ver}.zip
rm -rf ${WORKDIR}/sqlite-src-${sqlite3_ver}
( cd ${WORKDIR} ; unzip -x ${WORKDIR}/sqlite-src-${sqlite3_ver}.zip )

scl enable python27 rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/sqlite-src-\${sqlite3_ver}
./configure --prefix=${INSTALLDIR}/sqlite3 \
    --disable-tcl --disable-amalgamation --disable-readline --enable-debug \
    --enable-session \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -fuse-ld=lld" 
make -j5
make install
echo \${sqlite3_ver}>${INSTALLDIR}/sqlite3.ver
EOF




export cpptest_ver=2.0.0
# export cpptest_ver=1.1.2
cd ${WORKDIR}
wget https://sourceforge.net/projects/cpptest/files/cpptest/cpptest-${cpptest_ver}/cpptest-${cpptest_ver}.tar.gz/download \
    -O ${WORKDIR}/cpptest-${cpptest_ver}.tar.gz
tar -C ${WORKDIR} -xf ${WORKDIR}/cpptest-${cpptest_ver}.tar.gz
mkdir -p ${INSTALLDIR}/cpptest
scl enable devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib
cd ${WORKDIR}/cpptest-\${cpptest_ver}
./configure --prefix=${INSTALLDIR}/cpptest \
    --disable-dependency-tracking \
    CC=clang \
    "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++" \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -fuse-ld=lld" 
make -j5
make install
echo \${cpptest_ver}>${INSTALLDIR}/cpptest.ver
EOF







# ---------- GnuPG -----------------


wget https://gitlab.com/cluck/pkdl/-/archive/master/pkdl-master.tar.gz -O pkdl-master.tar.gz
tar -xf pkdl-master.tar.gz



cat >packages-2.2.17.current <<EOF
server https://gnupg.org/ftp/gcrypt

file gnupg/gnupg-2.2.17.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libgpg-error/libgpg-error-1.36.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libgcrypt/libgcrypt-1.8.5.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libksba/libksba-1.3.5.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libassuan/libassuan-2.5.3.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file ntbtls/ntbtls-0.1.2.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file npth/npth-1.6.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file pinentry/pinentry-1.1.0.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file gpgme/gpgme-1.13.1.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d


# https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.17.tar.bz2
# https://gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.36.tar.bz2
# https://gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.8.5.tar.bz2
# https://gnupg.org/ftp/gcrypt/libksba/libksba-1.3.5.tar.bz2
# https://gnupg.org/ftp/gcrypt/libassuan/libassuan-2.5.3.tar.bz2
# https://gnupg.org/ftp/gcrypt/ntbtls/ntbtls-0.1.2.tar.bz2
# https://gnupg.org/ftp/gcrypt/npth/npth-1.6.tar.bz2
# https://gnupg.org/ftp/gcrypt/pinentry/pinentry-1.1.0.tar.bz2
# https://gnupg.org/ftp/gcrypt/gpgme/gpgme-1.13.1.tar.bz2
EOF


cat > packages-2.2.16.current <<EOF
server https://gnupg.org/ftp/gcrypt

file gnupg/gnupg-2.2.16.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

# file libgpg-error/libgpg-error-1.36.tar.bz2
# chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libgcrypt/libgcrypt-1.8.5.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file libksba/libksba-1.3.5.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

# file libassuan/libassuan-2.5.3.tar.bz2
# chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file ntbtls/ntbtls-0.1.2.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d

file npth/npth-1.6.tar.bz2
chk 3709129724066088e30ad591b181e2433a4113bf31175a717451cbb94b2c679d






#
# GnuPG et al.
#
# All tarballs from this server are verified with an openpgp
# signature made by "Werner Koch (dist sig)".
# D869 2123 C406 5DEA 5E0F  3AB5 249B 39D2 4F25 E3B6
server ftp://ftp.gnupg.org/gcrypt

# last changed 2019-03-26
# by ah
# verified: distsigkey.gpg
server ftp://ftp.gnupg.org/gcrypt
file libgpg-error/libgpg-error-1.36.tar.bz2
chk babd98437208c163175c29453f8681094bcaf92968a15cafb1a276076b33c97c

# last-changed: 2019-03-26
# by: ah
# verified: distsigkey.gpg
file libassuan/libassuan-2.5.3.tar.bz2
chk  91bcb0403866b4e7c4bc1cc52ed4c364a9b5414b3994f718c70303f7f765e702

server ftp://ftp.gnupg.org/gcrypt

# TODO back to stable
# last changed: 2017-12-05
# by: ah
file pinentry/pinentry-1.1.0.tar.bz2
chk  68076686fa724a290ea49cdf0d1c0c1500907d1b759a3bcbfbec0293e8f56570

# last changed: 2019-03-26
# by: ah
# verified: distsigkey.gpg
file gpgme/gpgme-1.13.0.tar.bz2
chk  d4b23e47a9e784a63e029338cce0464a82ce0ae4af852886afda410f9e39c630

EOF

#
scl enable rh-python35 'bash -vxe' <<EOF
export SPKDL_CMD=( python "$PWD/pkdl-master/spkdl.py")
\${SPKDL_CMD[@]} -C -x -c packages-2.2.16.current

EOF



cat > gpg4win.current <<EOF
server https://files.gpg4win.org/
file gpg4win-3.1.10.tar.bz2
chk 9b843e2443f454ce9fb7e1ad223f861b3f2897e8d597e377824b960fa79e2ab6
EOF
scl enable rh-python35 'bash -vxe' <<EOF
export SPKDL_CMD=( python "$PWD/pkdl-master/spkdl.py")
\${SPKDL_CMD[@]} -C -x -c gpg4win.current

EOF


yum -y install mingw{32,64}-gcc{,-c++,-objc++,-objc} mingw32-winpthreads stow texinfo-tex ghostscript ImageMagick cmake

scl enable python27 devtoolset-8 bash
cp -ar src/playground/build/gpg4win-3.1.10 ~/gpg4win-3.1.10 

export PATH=/root/opt/llvm/bin:/root/opt/autotools/bin:$PATH
export LD_RUN_PATH=/root/opt/llvm/lib:/root/opt/autotools/lib
export LD_LIBRARY_PATH=/root/opt/llvm/lib
# export PKG_CONFIG_PATH=
# For debugging:
export CFLAGS="-Og -ggdb"
export CXXFLAGS="-Og -ggdb"
# export LDFLAGS="\${LDFLAGS}"








      # libgpg-error/1.36 \
      # libassuan/2.5.3 \
      # libgcrypt/1.8.5 \
      # libksba/1.3.5 \
      # npth/1.6 \
      # ntbtls/0.1.2 \
      # gnupg/2.2.17 \
      # gpgme/1.13.1 \
      # pinentry/1.1.0 \


_libprefix=lib
_libsuffix=-mt-x64


export WORKDIR=/root
export INSTALLDIR=/root/opt

export PLAYGROUND_BASE=/root/src/playground

parent_pkgs=()
: >/root/src/playground/enable
for _THE_PKG in \
      libgpg-error/1.36 \
      libassuan/2.5.3 \
      libgcrypt/1.8.5 \
      libksba/1.3.5 \
      npth/1.6 \
      ntbtls/0.1.2 \
      gnupg/2.2.16 \
      gpgme/1.13.0 \
      pinentry/1.1.0 \
; do
  THE_PKG_NAME=${_THE_PKG%%/*}
  THE_PKG_VER=${_THE_PKG#*/}
  THE_PKG=${THE_PKG_NAME}-${THE_PKG_VER}
  
  scl enable devtoolset-8 'bash -vxe' <<EOF
set -e
export WORKDIR=/root/
PLAYTARGET=${PLAYGROUND_BASE}/install/${THE_PKG_NAME}/${THE_PKG_VER}/
PLAYBUILD=${PLAYGROUND_BASE}/build/${THE_PKG}/
export PATH=${INSTALLDIR}/llvm/bin:${WORKDIR}/opt/autotools/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib:${WORKDIR}/opt/autotools/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:${PLAYTARGET}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/cpptest/lib/pkgconfig:/opt/rh/python27:${WORKDIR}/usr/lib64/pkgconfig
# For debugging:
export CFLAGS="-Og -ggdb \${CFLAGS}"
export CXXFLAGS="-Og -ggdb \${CXXFLAGS}"
# export LDFLAGS="\${LDFLAGS}"

. /root/src/playground/enable

cd \${PLAYBUILD}
if ! [ -d \${PLAYTARGET}bin -o -d \${PLAYTARGET}lib ] ; then
  CF=( --prefix=\${PLAYTARGET} )
  case $THE_PKG_NAME in
    gnupg)
        export LDFLAGS="-lrt\${LDFLAGS:+:${LDFLAGS}}"
        ./configure \${CF[@]} CPPFLAGS=-DIN_EXCL_UNLINK=0x04000000
        ;;
    pinentry) ./configure \${CF[@]} --enable-pinentry-tty --with-ncurses-include-dir=/usr/include ;;
    *) ./configure \${CF[@]} ;;
  esac
  make -j8
  make install
  case $THE_PKG_NAME in
  gnupg)
    ( cd tools ; make ; make install ; ) ;;
  esac
fi
# mkdir -p /root/src/playground/install-ex
# export DESTDIR=/root/src/playground/install-ex
# make install

EOF
[ $? -eq 0 ] || break
# --enable-pinentry-curses --enable-fallback-curses

  parent_pkgs+=( "${_THE_PKG}" )
  _PATH=""
  _LD_LIBRARY_PATH=""
  _LD_RUN_PATH=""
  _PKG_CONFIG_PATH=""
  _CPPFLAGS=""
  _CFLAGS=""
  _CXXFLAGS=""
  _LDFLAGS=""
  _LDLIBS=""
  for _THE_PKG in ${parent_pkgs[@]} ; do
      THE_PKG_NAME=${_THE_PKG%%/*}
      THE_PKG_VER=${_THE_PKG#*/}
      THE_PKG=${THE_PKG_NAME}-${THE_PKG_VER}

      installdir=${PLAYGROUND_BASE}/install/${THE_PKG_NAME}/${THE_PKG_VER}/
      echo HIHI $THE_PKG $_PATH "'$THE_PKG_NAME'"

      # case $THE_PKG_NAME in
      # gnupg)
      #   _CPPFLAGS="-DIN_EXCL_UNLINK=0x04000000"${_CPPFLAGS:+ ${_CPPFLAGS}} ;;
      # esac

      _PATH=${installdir}bin${_PATH:+:${_PATH}}
      _LD_LIBRARY_PATH=${installdir}lib${_LD_LIBRARY_PATH:+:${_LD_LIBRARY_PATH}}
      _LD_RUN_PATH==${installdir}lib${_LD_RUN_PATH:+:${_LD_RUN_PATH}}
      _PKG_CONFIG_PATH=${installdir}lib/pkgconfig${_PKG_CONFIG_PATH:+:${_PKG_CONFIG_PATH}}
      # _CPPFLAGS=${installdir}include${_CFLAGS:+ ${_CFLAGS}}
      if [ -d "${installdir}include" ] ; then
        _CFLAGS=-I${installdir}include${_CFLAGS:+ ${_CFLAGS}}
        _CXXFLAGS=-I${installdir}include${_CXXFLAGS:+ ${_CXXFLAGS}}
        # _LDFLAGS=-I${installdir}include${_LDFLAGS:+ ${_LDLAGS}}
      fi

      case ${THE_PKG_NAME} in
      gnupg)
        # _CPPFLAGS="-DIN_EXCL_UNLINK=0x04000000"${_CPPFLAGS:+ ${_CPPFLAGS}} ;;
        # _LDFLAGS=-lrt${_LDFLAGS:+ ${_LDLAGS}}
        _LDLIBS=-lrt${_LDLIBS:+ ${_LDLIBS}}
        #_CFLAGS=-lrt${_CFLAGS:+ ${_CFLAGS}}
        # _CXXFLAGS=-lrt${_CXXFLAGS:+ ${_CXXFLAGS}}
        ;;
      esac
  done
  cat >/root/src/playground/enable <<EOF
export PATH=${_PATH}\${PATH:+:\${PATH}}
export LD_LIBRARY_PATH=${_LD_LIBRARY_PATH}\${LD_LIBRARY_PATH:+:\${LD_LIBRARY_PATH}}
export LD_RUN_PATH=${_LD_RUN_PATH}\${LD_RUN_PATH:+:\${LD_RUN_PATH}}
export PKG_CONFIG_PATH=${_PKG_CONFIG_PATH}\${PKG_CONFIG_PATH:+:\${PKG_CONFIG_PATH}}
export CFLAGS="${_CFLAGS}\${CFLAGS:+:\${CFLAGS}}"
export CXXFLAGS="${_CXXFLAGS}\${CXXFLAGS:+:\${CXXFLAGS}}"
export CPPFLAGS="${_CPPFLAGS}\${CPPFLAGS:+:\${CPPFLAGS}}"
export LDFLAGS="${_LDFLAGS}\${LDFLAGS:+:\${LDFLAGS}}"
export LDLIBS="${_LDLIBS}\${LDLIBS:+:\${LDLIBS}}"
# export CC=clang
# export CXX=clang++ -stdlib=libc++ -std=c++11
# export CFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 \${CFLAGS} -stdlib=libc++
# export CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -I/opt/rh/devtoolset-8/root/usr/include/c++/8 -I/opt/rh/devtoolset-8/root/usr/include/c++/8/x86_64-redhat-linux/ \${CXXFLAGS} -stdlib=libc++
EOF

  cat /root/src/playground/enable
done






























# ===========================  Sequoia Variant

# sequoia april 24   16:53   1d35557a8f7866141f84b2764287f384a8e12872

cd ${WORKDIR}/pEpEngine
# hg update -Cr sync
# works::::  hg up -Cr 0399738decf9
# Currently: hg id -> 7c757957849b+ (sync)

# commit at elca: 0399738decf9 (sync)


# parent:      3539:d6dc50b087af
# user:        Krista 'DarthMama' Bennett <krista@pep.foundation>
# date:        Mon Apr 29 12:15:05 2019 +0200
# summary:     delete key tests for Neal - this will suck for automated gpg tests, sorry damiano
# #
hg up -Cr d6dc50b087af
hg update -Cr 01cde6481916   # Engine JSON release 1.0.24


# hg pull
# hg update -Cr sync
hg update -Cr ENGINE-524
hg purge -X local.conf --all --config extensions.purge=


# hg up -Cr ENGINE-524
# hg merge sync
# # interact ...
# hg resolve -m
# HGUSER="Claudio Luck" EMAIL="claudio.luck@pep.foundation" hg commit -m "merge sync"


cd ${WORKDIR}/pEpEngine
export engine_ver=$(x=$(hg id -i) ; echo ${x/+/})



# GnuPG Variant







# Engine SEQUOIA
scl enable python27 rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/sqlite3/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64:/opt/rh/rh-python35:${WORKDIR}/usr/lib64
# - /opt/rh/rh-python35${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/share/pkgconfig:${INSTALLDIR}/nettle/lib64/pkgconfig:${INSTALLDIR}/capnp/lib/pkgconfig:${INSTALLDIR}/cpptest/lib/pkgconfig:${INSTALLDIR}/nettle/lib64/pkgconfig:${INSTALLDIR}/curl/lib/pkgconfig::/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/pEpEngine
# https://files.pythonhosted.org/packages/89/51/a8a6cdb8a084d32dbc9bda94623dc35310ae2002be57de8702a1703c0026/lxml-4.3.3-cp27-cp27mu-manylinux1_x86_64.whl
python2 -c 'import lxml' || pip install lxml
sed -i 's/ -pdu=PEP.Message//' asn.1/Makefile  || true
sed -i 's/rm -f converter-sample.c/rm -f converter-example.c/' asn.1/Makefile  || true
function LWl () { echo "-L\$1 -Wl,-rpath,\$1" ; }
cat >local.conf <<__LOCAL__
# SYSTEM_DB = "system.db"
PREFIX    = ${INSTALLDIR}/pep
OPENPGP   = SEQUOIA
CC        = clang
CXX       = clang++ -stdlib=libc++ -std=c++11
# CXX       = g++
# CXXFLAGS  = -I${INSTALLDIR}/llvm/include/c++/v1 -I${WORKDIR}/pEpEngine/src -stdlib=libc++

# CXXFLAGS  = -isystem /opt/rh/devtoolset-8/root/usr/include/c++/8 -I${WORKDIR}/pEpEngine/src

CXXFLAGS  = -I${INSTALLDIR}/llvm/include/c++/v1 -I/opt/rh/devtoolset-8/root/usr/include/c++/8  -I/opt/rh/devtoolset-8/root/usr/include/c++/8/x86_64-redhat-linux/  -I${WORKDIR}/pEpEngine/src -stdlib=libc++

CPPFLAGS += -USYSTEM_DB -isystem ${INSTALLDIR}/asn1c/share/asn1c -I${INSTALLDIR}/sqlite3/include -I${INSTALLDIR}/libetpan/include -isystem ${INSTALLDIR}/pep/include -isystem /usr/include
LDFLAGS  += \$(LWl ${INSTALLDIR}/llvm/lib) \$(LWl ${INSTALLDIR}/sequoia/lib) \$(LWl ${INSTALLDIR}/libetpan/lib) \$(LWl ${INSTALLDIR}/sqlite3/lib) \$(LWl ${INSTALLDIR}/pep/lib) -fuse-ld=lld -nostartfiles
YML2_PROC = python2 ${WORKDIR}/yml2_checkout/yml2proc
YML2_PATH = ${WORKDIR}/yml2_checkout
CPPUNIT_INC = -I${INSTALLDIR}/cpptest/include
CPPUNIT_LIB = -L${INSTALLDIR}/cpptest/lib
# \$(LWl ${INSTALLDIR}/cpptest/lib)

# TestDriver: CXXFLAGS += -I${WORKDIR}/pEpEngine/src
# TestDriver: LDFLAGS += \\\$(LDFLAGS) -L${INSTALLDIR}/llvm/lib

# undefine SYSTEM_DB
# SYSTEM_DB = unix_system_db("/usr/share/pEp")
__LOCAL__
# make clean
# For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here,
#  instead add them to local.conf ( yes, home-cooked build-system :-/ )
make WARN= DEBUG= 
make WARN= DEBUG= db
make WARN= DEBUG= install
# make WARN= DEBUG= test
mkdir -p ${INSTALLDIR}/pep/share/pEp
install -m 0644 db/system.db ${INSTALLDIR}/pep/share/pEp/system.db
echo \${engine_ver}>${INSTALLDIR}/pEp_engine.ver
EOF





# Engine GnuPG
scl enable python27 rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/sqlite3/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64:/opt/rh/rh-python35:${WORKDIR}/usr/lib64
# - /opt/rh/rh-python35${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/cpptest/lib/pkgconfig:/opt/rh/python27
# . /root/src/playground/enable
cd ${WORKDIR}/pEpEngine
# https://files.pythonhosted.org/packages/89/51/a8a6cdb8a084d32dbc9bda94623dc35310ae2002be57de8702a1703c0026/lxml-4.3.3-cp27-cp27mu-manylinux1_x86_64.whl
python2 -c 'import lxml' || pip install lxml
sed -i 's/ -pdu=PEP.Message//' asn.1/Makefile  || true
sed -i 's/rm -f converter-sample.c/rm -f converter-example.c/' asn.1/Makefile  || true
function LWl () { echo "-L\$1 -Wl,-rpath,\$1" ; }
cat >local.conf <<__LOCAL__
# SYSTEM_DB = "system.db"
PREFIX    = ${INSTALLDIR}/pep
OPENPGP   = GPG
CC        = clang
CXX       = clang++ -stdlib=libc++ -std=c++11
# CXX       = g++
# CXXFLAGS  = -I${INSTALLDIR}/llvm/include/c++/v1 -I${WORKDIR}/pEpEngine/src -stdlib=libc++

# CXXFLAGS  = -isystem /opt/rh/devtoolset-8/root/usr/include/c++/8 -I${WORKDIR}/pEpEngine/src

CXXFLAGS  = -I${INSTALLDIR}/llvm/include/c++/v1 -I/opt/rh/devtoolset-8/root/usr/include/c++/8  -I/opt/rh/devtoolset-8/root/usr/include/c++/8/x86_64-redhat-linux/  -I${WORKDIR}/pEpEngine/src -stdlib=libc++

CPPFLAGS += -USYSTEM_DB -isystem ${INSTALLDIR}/asn1c/share/asn1c -I${INSTALLDIR}/sqlite3/include -I${INSTALLDIR}/libetpan/include -isystem ${INSTALLDIR}/pep/include -isystem /usr/include
LDFLAGS  += \$(LWl ${INSTALLDIR}/llvm/lib) \$(LWl ${INSTALLDIR}/libetpan/lib) \$(LWl ${INSTALLDIR}/sqlite3/lib) \$(LWl ${INSTALLDIR}/pep/lib) -fuse-ld=lld -nostartfiles
YML2_PROC = python2 ${WORKDIR}/yml2_checkout/yml2proc
YML2_PATH = ${WORKDIR}/yml2_checkout
CPPUNIT_INC = -I${INSTALLDIR}/cpptest/include
CPPUNIT_LIB = -L${INSTALLDIR}/cpptest/lib

GPGME_INC = -I${WORKDIR}/src/playground/install/gpgme/1.13.1/include -I${WORKDIR}/src/playground/install/libgpg-error/1.36/include
GPGME_LIB = -L${WORKDIR}/src/playground/install/gpgme/1.13.1/lib -L${WORKDIR}/src/playground/install/libgpg-error/1.36/lib

# \$(LWl ${INSTALLDIR}/cpptest/lib)

# TestDriver: CXXFLAGS += -I${WORKDIR}/pEpEngine/src
# TestDriver: LDFLAGS += \\\$(LDFLAGS) -L${INSTALLDIR}/llvm/lib

# undefine SYSTEM_DB
# SYSTEM_DB = unix_system_db("/usr/share/pEp")
__LOCAL__
# make clean
# For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here,
#  instead add them to local.conf ( yes, home-cooked build-system :-/ )
make WARN= DEBUG= 
make WARN= DEBUG= db
make WARN= DEBUG= install
# make WARN= DEBUG= test
mkdir -p ${INSTALLDIR}/pep/share/pEp
install -m 0644 db/system.db ${INSTALLDIR}/pep/share/pEp/system.db
echo \${engine_ver}>${INSTALLDIR}/pEp_engine.ver
EOF









cd ${WORKDIR}/libpEpAdapter

# hg update -Cr default
hg update -Cr fd5d91cc2250

# Currently: hg id -> fd5d91cc2250+ (sync)
hg purge -X local.conf --all --config extensions.purge=

cd ${WORKDIR}/libpEpAdapter
export libpepa_ver=$(x=$(hg id -i) ; echo ${x/+/})

scl enable python27 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/libpEpAdapter

cat >local.conf <<__LOCAL__
PREFIX    = ${INSTALLDIR}/libpepa
CC        = clang
CXX       = clang++ -stdlib=libc++
CXXFLAGS += -I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++
CPPFLAGS += -isystem ${INSTALLDIR}/pep/include  # -isystem /usr/include
LDFLAGS  += -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -fuse-ld=lld -nostartfiles
# YML2_PROC = ${WORKDIR}/yml2_checkout/yml2proc
# YML2_PATH = ${WORKDIR}/yml2_checkout
__LOCAL__
make clean
# For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here,
#  instead add them to local.conf ( yes, home-cooked build-system :-/ )
make WARN= DEBUG= 
make WARN= DEBUG= install
echo \${libpepa_ver}>${INSTALLDIR}/pEp_libadapter.ver
EOF




cd ${WORKDIR}/pEpJNIAdapter
# hg update -Cr default
# hg update -Cr fd5d91cc2250
# Currently: hg id -> fd5d91cc2250+ (sync)

hg up -Cr sync
# Currently: ae3dd2c8065f (sync) tip
hg purge -X local.conf --all --config extensions.purge=


cd ${WORKDIR}/pEpJNIAdapter
export pepjni_ver=$(x=$(hg id -i) ; echo ${x/+/})

scl enable python27 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/pEpJNIAdapter/src
mkdir -p ${INSTALLDIR}/home/java

# ln -sf ${INSTALLDIR}/pep/include/pEp/sync_api.h ${INSTALLDIR}/pep/include/pEp/sync.h
# sed -i 's/sync.h/Sync.h/' org_pEp_jniadapter_AbstractEngine.cc  || true
# sed -i 's/sync.h/Sync.h/' basic_api.cc  || true

cat >local.conf <<__LOCAL__
PREFIX    = ${INSTALLDIR}/pep
JAVA_HOME = /usr/lib/jvm/java-1.7.0-openjdk.x86_64
ENGINE_INC= -I${INSTALLDIR}/pep/include
ENGINE_LIB= -L${INSTALLDIR}/pep/lib
AD_INC    = 
AD_LIB    = -L${INSTALLDIR}/pep/lib

CC        = clang
CXX       = clang++ -stdlib=libc++
CXXFLAGS += -I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++ -isystem ${INSTALLDIR}/asn1c/share/asn1c \\
    -isystem ${INSTALLDIR}/libpepa/include -isystem ${INSTALLDIR}/pep/include
# CPPFLAGS  = -isystem ${INSTALLDIR}/pep/include # -isystem /usr/include
# -isystem ${INSTALLDIR}/pep/include/pEp 
CFLAGS   += -isystem ${INSTALLDIR}/libpepa/include -isystem ${INSTALLDIR}/pep/include -isystem ${INSTALLDIR}/asn1c/share/asn1c

# -isystem ${INSTALLDIR}/libpepa/include -isystem ${INSTALLDIR}/pep/include
LDFLAGS  += \\
    -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/libpepa/lib -Wl,-rpath,${INSTALLDIR}/libpepa/lib -L${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/pep/lib \\
    -UDISABLE_SYNC -fuse-ld=lld

# LDFLAGS   = -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -fuse-ld=lld
# -nostartfiles
YML2_PROC = ${WORKDIR}/yml2_checkout/yml2proc
YML2_PATH = ${WORKDIR}/yml2_checkout
__LOCAL__
# make clean
# For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here,
#  instead add them to local.conf ( yes, home-cooked build-system :-/ )
make WARN= DEBUG=
cd ${WORKDIR}/pEpJNIAdapter/test
make

# make WARN= DEBUG= install
cd ${WORKDIR}/pEpJNIAdapter
install -m 644 -t ${INSTALLDIR}/pep/lib src/libpEpJNI.a
install -m 755 -t ${INSTALLDIR}/pep/lib src/libpEpJNI.so
install -m 644 -t ${INSTALLDIR}/pep/lib src/pEp.jar
echo \${pepjni_ver}>${INSTALLDIR}/pEp_JNI.ver
EOF



# NOTE: make test
# /bin/sh: time: command not found
yum -y install time





# ===========================
# Aux JSON (Mini?) Server
# ===========================

yum -y install libicu libicu-devel

export boost_ver=1.70.0
export boost_ver_uscore=1_70_0
cd ${WORKDIR}
scl enable rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/rh-python35${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/rh-python35${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/boost_\${boost_ver_uscore}
mkdir -p ${INSTALLDIR}/boost.stage ${INSTALLDIR}/boost.debug

rm -f project-config.jam*
./bootstrap.sh --with-icu=/usr \
    --with-toolset=clang \
    --prefix=${INSTALLDIR}/boost \
    --with-python-version=3.5
args=(
    -d+2 -q -j 3 
    --layout=tagged
    runtime-link=shared threading=multi
    toolset=clang variant=release
    architecture=x86 address-model=64
  )
argsR35=(
    --with-program_options --with-filesystem --with-system --with-thread --with-date_time --with-python --with-locale
    cxxflags="-v -stdlib=libc++ \$(pkg-config --cflags python-3.5)"
    linkflags="-v -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib \$(pkg-config --libs-only-L python-3.5) -Wl,-rpath,${INSTALLDIR}/boost/lib -fuse-ld=lld -stdlib=libc++"
    runtime-debugging=off debug-symbols=off
  )
argsD35=(
    --with-program_options --with-filesystem --with-system --with-thread --with-date_time --with-python --with-locale
    cxxflags="-v -stdlib=libc++ \$(pkg-config --cflags python-3.5)"
    linkflags="-v -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib \$(pkg-config --libs-only-L python-3.5) -Wl,-rpath,${INSTALLDIR}/boost.debug/lib -fuse-ld=lld -stdlib=libc++"
    runtime-debugging=on debug-symbols=on
  )

./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR35[@]}" link=shared  --clean
# ./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsD35[@]}" link=static  --clean

# ./b2 --stagedir=${INSTALLDIR}/boost.stage "\${args[@]}" "\${argsR35[@]}" link=static  clean
# ./b2 --stagedir=${INSTALLDIR}/boost.stage "\${args[@]}" "\${argsR35[@]}" link=static  stage
# ./b2 --stagedir=${INSTALLDIR}/boost.stage "\${args[@]}" "\${argsR35[@]}" link=static  install

# ./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR35[@]}" link=static  stage
./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR35[@]}" link=shared  stage
# ./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsD35[@]}" link=shared  stage

# L=${INSTALLDIR}/boost/lib/libboost_python35-mt-x64.so.1.70.0
# rp=\$(patchelf --print-rpath "\$L")
# patchelf --set-rpath "\$rp:/opt/rh/rh-python35${WORKDIR}/usr/lib64" "\$L"

# =============
EOF

scl enable python27 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/boost_\${boost_ver_uscore}
mkdir -p ${INSTALLDIR}/boost.stage ${INSTALLDIR}/boost.debug

rm -f project-config.jam*
./bootstrap.sh --with-icu=/usr \
    --with-toolset=clang \
    --prefix=${INSTALLDIR}/boost \
    --with-python=\$(which python) --with-python-root=/opt/rh/python27${WORKDIR}/usr

args=(
    -d+2 -q -j 3 
    --layout=tagged
    runtime-link=shared threading=multi
    toolset=clang variant=release
    architecture=x86 address-model=64
  )
argsR27=(
    --with-python
    cxxflags="-v -stdlib=libc++ \$(pkg-config --cflags python-2.7)"
    linkflags="-v -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib \$(pkg-config --libs-only-L python-2.7) -Wl,-rpath,${INSTALLDIR}/boost/lib -fuse-ld=lld -stdlib=libc++"
    runtime-debugging=off debug-symbols=off
  )
argsD27=(
    --with-python
    cxxflags="-v -stdlib=libc++ \$(pkg-config --cflags python-2.7)"
    linkflags="-v -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib \$(pkg-config --libs-only-L python-2.7) -Wl,-rpath,${INSTALLDIR}/boost.debug/lib -fuse-ld=lld -stdlib=libc++"
    runtime-debugging=on debug-symbols=on
  )

./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR27[@]}" link=shared  --clean
# ./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsD27[@]}" link=static  --clean

# ./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR27[@]}" link=static  stage
./b2 --stagedir=${INSTALLDIR}/boost "\${args[@]}" "\${argsR27[@]}" link=shared  stage

L=${INSTALLDIR}/boost/lib/libboost_python27-mt-x64.so.1.70.0
rp=\$(patchelf --print-rpath "\$L")
patchelf --set-rpath "\$rp:/opt/rh/python27${WORKDIR}/usr/lib64" "\$L"

# echo \${boost_ver}>${INSTALLDIR}/boost.ver
EOF




cd ${WORKDIR}/pEpJSONServerAdapter
hg update -Cr JSON-123

hg update -Cr 97c2a3b7e885   # JSON Server release 1.0.24
hg purge -X local.conf --all --config extensions.purge=

cd ${WORKDIR}/pEpJSONServerAdapter
export json_ver=$(x=$(hg id -i) ; echo ${x/+/})

scl enable python27 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/sqlite3/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64
# - /opt/rh/rh-python35${WORKDIR}/usr/lib64
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/share/pkgconfig:${INSTALLDIR}/nettle/lib64/pkgconfig:${INSTALLDIR}/capnp/lib/pkgconfig:${INSTALLDIR}/nettle/lib64/pkgconfig:${INSTALLDIR}/curl/lib/pkgconfig::/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/pEpJSONServerAdapter

# https://files.pythonhosted.org/packages/89/51/a8a6cdb8a084d32dbc9bda94623dc35310ae2002be57de8702a1703c0026/lxml-4.3.3-cp27-cp27mu-manylinux1_x86_64.whl

function LWl () { echo "-L\$1 -Wl,-rpath,\$1" ; }

cd libevent-2.0.22-stable
if true ; then
mkdir -p "${INSTALLDIR}/libevent"
./configure --prefix="${INSTALLDIR}/libevent"/ \
    --disable-openssl --disable-malloc-replacement \
    CC=clang \
    CXX=clang++ "CXXFLAGS=-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++ " \
    "LDFLAGS=-L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -fuse-ld=lld"
make -j5 all
make install
fi

cd ../server

cat >local.conf <<__LOCAL__
# SYSTEM_DB = "system.db"
PREFIX    = ${INSTALLDIR}/pep

YML2_PROC = ${WORKDIR}/yml2_checkout/yml2proc
YML2_PATH = ${WORKDIR}/yml2_checkout
GTEST_DIR = ${WORKDIR}/gtest/googletest
BOOST_INC = -I${INSTALLDIR}/boost/include
BOOST_LIB = -L${INSTALLDIR}/boost/lib -Wl,-rpath,${INSTALLDIR}/boost/lib
ENGINE_INC= -I${INSTALLDIR}/pep/include
ENGINE_LIB= -L${INSTALLDIR}/pep/lib -Wl,-rpath,${INSTALLDIR}/pep/lib
EVENT_INC = -I${INSTALLDIR}/libevent/include
EVENT_LIB = -L${INSTALLDIR}/libevent/lib
# This is the one we want the source code to know (we override it while "install")
HTML_DIRECTORY = ${INSTALLDIR}/pep/share/pEp/html

CC        = clang
CXX       = clang++ -pthread -fPIC -stdlib=libc++
# server/Makefile.conf:    CXXFLAGS=-pthread -fstack-protector-all -fcolor-diagnostics
CXXFLAGS += -I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++
CPPFLAGS += -isystem ${INSTALLDIR}/libpepa/include -isystem ${INSTALLDIR}/pep/include -I${WORKDIR}/gtest/googletest/include -I${WORKDIR}/boost_1_70_0    # -isystem /usr/include
LDFLAGS  += -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -L${INSTALLDIR}/libpepa/lib -fuse-ld=lld
__LOCAL__
# make clean

cat >prefix-config.cc <<'PATCH'
// This file is generated by make. Edit the Makefile, not this file!
#include "prefix-config.hh"
const char* const html_directory = "html";
PATCH

if ! grep -q mt-x64 Makefile.conf ; then
sed -i "s#-lboost_system#-lboost_system-mt-x64#" Makefile.conf
sed -i "s#-lboost_filesystem#-lboost_filesystem-mt-x64#" Makefile.conf
sed -i "s#-lboost_program_options#-lboost_program_options-mt-x64#" Makefile.conf
sed -i "s#-lboost_thread#-lboost_thread-mt-x64#" Makefile.conf
fi

make -j3 WARN= DEBUG= 
make -j3 WARN= DEBUG= HTML_DIRECTORY=${INSTALLDIR}/pep/share/pEp/html install
echo \${json_ver}>${INSTALLDIR}/pEp_mini_JSON.ver
EOF






cd ${WORKDIR}/pEpPythonAdapter
hg up -Cr sync

cat >setup.py <<'EOF'
# -*- coding: utf-8 -*-

# This file is under GNU Affero General Public License 3.0
# see LICENSE.txt

from setuptools import setup, Extension
from glob import glob
from os import environ, uname
from os.path import dirname, exists, join
from sys import argv

compile_args = ['-O0', '-g', '-UNDEBUG', '-std=c++14'] \
        if '--debug' in argv or '-g' in argv else ['-std=c++14']

ENGINE_INC = environ['ENGINE_INC']
ENGINE_LIB = environ['ENGINE_LIB']

BOOST_INC = environ['BOOST_INC']
BOOST_LIB = environ['BOOST_LIB']

ASN1C_INC = environ['ASN1C_INC']

module_pEp = Extension('pEp',
        sources = glob('src/*.cc'),
        libraries = ['pEpEngine', 'boost_python35-mt-x64', 'boost_locale-mt-x64',],
        extra_compile_args = compile_args,
        include_dirs = [ENGINE_INC, BOOST_INC, ASN1C_INC],
        library_dirs = [ENGINE_LIB, BOOST_LIB],
    )

setup(
        name='pEp',
        version='2.0',
        description='p≡p for Python',
        author="Volker Birk",
        author_email="vb@pep-project.org",
        ext_modules=[module_pEp,],
    )
EOF



cd ${WORKDIR}/pEpPythonAdapter
export pypep_ver=$(x=$(hg id -i) ; echo ${x/+/})

scl enable rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/rh-python35${WORKDIR}/usr/lib64:${INSTALLDIR}/boost/lib
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/rh-python35${WORKDIR}/usr/lib64/pkgconfig
cd ${WORKDIR}/pEpPythonAdapter

# ---- >>-*-<< very specific for this package >>-*-<< ---- 
export CC="clang++ -stdlib=libc++"  # <<------ for this python setup.py only... #$%^&*
export CXX="clang++ -stdlib=libc++"
export CXXFLAGS="-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++"
export LDFLAGS="-stdlib=libc++ -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/boost/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -fuse-ld=lld"

export ENGINE_INC=${INSTALLDIR}/pep/include
export ENGINE_LIB=${INSTALLDIR}/pep/lib
export BOOST_INC=${WORKDIR}/boost_1_70_0
export BOOST_LIB=${INSTALLDIR}/boost/lib
export ASN1C_INC=${INSTALLDIR}/asn1c/share

rm -rf build dist
python setup.py build

L=build/lib.linux-x86_64-3.5/pEp.cpython-35m-x86_64-linux-gnu.so
rp=\$(patchelf --print-rpath "\$L")

# /opt/rh/rh-python35${WORKDIR}/usr/lib64/python3.5/site-packages/pEp-2.0-py3.5-linux-x86_64.egg/pEp.cpython-35m-x86_64-linux-gnu.so
patchelf --set-rpath "\\\$ORIGIN/../lib:\\\$ORIGIN/../../.." "\$L"
python setup.py bdist_egg
mkdir -p ${INSTALLDIR}/pep/lib
install -m 0644 dist/pEp-2.0-py3.5-linux-x86_64.egg ${INSTALLDIR}/pep/share/pEp

patchelf --set-rpath "\$rp:/opt/rh/rh-python35${WORKDIR}/usr/lib64" "\$L"
mkdir -p ${INSTALLDIR}/pep/lib
install -m 0644 "\$L" ${INSTALLDIR}/pep/lib
cd dist
rm -f pEp.py
unzip pEp-2.0-py3.5-linux-x86_64.egg pEp.py </dev/null
install -m 0644 pEp.py ${INSTALLDIR}/pep/lib

echo \${pypep_ver}>${INSTALLDIR}/pEp_python.ver
EOF


# Currently(?) fails to compile with Python 2.7:
#
# cd ${WORKDIR}/pEpPythonAdapter
# export pypep_ver=$(x=$(hg id -i) ; echo ${x/+/})
# scl enable python27 devtoolset-8 'bash -vxe' <<EOF
# export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
# export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
# export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/python27${WORKDIR}/usr/lib64:${INSTALLDIR}/boost/lib
# export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/python27${WORKDIR}/usr/lib64/pkgconfig
# cd ${WORKDIR}/pEpPythonAdapter

# # ---- >>-*-<< very specific for this package >>-*-<< ---- 
# export CC="clang++ -stdlib=libc++"  # <<------ for this python setup.py only... #$%^&*
# export CXX="clang++ -stdlib=libc++"
# export CXXFLAGS="-I${INSTALLDIR}/llvm/include/c++/v1 -stdlib=libc++"
# export LDFLAGS="-stdlib=libc++ -L${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/llvm/lib -Wl,-rpath,${INSTALLDIR}/boost/lib -Wl,-rpath,${INSTALLDIR}/pep/lib -fuse-ld=lld"

# export ENGINE_INC=${INSTALLDIR}/pep/include
# export ENGINE_LIB=${INSTALLDIR}/pep/lib
# export BOOST_INC=${WORKDIR}/boost_1_70_0
# export BOOST_LIB=${INSTALLDIR}/boost/lib
# export ASN1C_INC=${INSTALLDIR}/asn1c/share

# rm -rf build dist
# python setup.py build

# EOF




# extra_compile_args
# extra_link_args

scl enable rh-python35 devtoolset-8 'bash -vxe' <<EOF
export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:\$PATH
# export PATH=${INSTALLDIR}/asn1c/bin:${INSTALLDIR}/llvm/bin:$PATH
export LD_RUN_PATH=${INSTALLDIR}/llvm/lib
export LD_LIBRARY_PATH=${INSTALLDIR}/llvm/lib:/opt/rh/rh-python35${WORKDIR}/usr/lib64:${INSTALLDIR}/boost/lib
export PKG_CONFIG_PATH=${INSTALLDIR}/pep/lib/pkgconfig:${INSTALLDIR}/sequoia/usr/share/pkgconfig:/opt/rh/rh-python35${WORKDIR}/usr/lib64/pkgconfig

# creating /opt/rh/rh-python35${WORKDIR}/usr/lib64/python3.5/site-packages/pEp-2.0-py3.5-linux-x86_64.egg

rm -rf /opt/rh/rh-python35${WORKDIR}/usr/lib64/python3.5/site-packages/pEp-2.0-py3.5-linux-x86_64.egg

easy_install dist/pEp-2.0-py3.5-linux-x86_64.egg

readelf -d /opt/rh/rh-python35${WORKDIR}/usr/lib/python3.5/site-packages/pEp-2.0-py3.5-linux-x86_64.egg/pEp.cpython-35m-x86_64-linux-gnu.so

LD_LIBRARY_PATH=${INSTALLDIR}/pep/lib python -c 'import pEp ; print(pEp.__file__)'
EOF






# ===========================
# Distro
# ===========================

echo 7 >"${WORKDIR}/D_REVISION"

D_REV=$(cat ${WORKDIR}/D_REVISION)
D=""

D=${WORKDIR}/out
# trap '[ -z "$D" ] || rm -rf "$D"' EXIT
#D=$(mktemp -d)

mkdir -p ${WORKDIR}/out
rm -rf ${WORKDIR}/out/*
# pep  asn1c  capnp  cmake  curl  gmp  llvm  nettle  ninja  sequoia
# bin  include  lib  lib64  libexec  share
mkdir -p "$D"/{bin,lib/pEp,share/pEp,include/pEp}
# cp -a ${INSTALLDIR}/pep/bin/sqlite3 "$D"/bin

# Engine and below, and libpEpAdapter
cp -a ${INSTALLDIR}/pep/lib/libpEpEngine.so "$D"/lib
cp -ar ${INSTALLDIR}/libetpan/lib/libetpan.so* "$D"/lib/pEp
cp -ar ${INSTALLDIR}/sqlite3/lib/libsqlite3.so* "$D"/lib/pEp

# cp -arv ${INSTALLDIR}/sqlite3/include/. "$D"/include
cp -arv ${INSTALLDIR}/pep/include/pEp/. "$D"/include/pEp
cp -arv ${INSTALLDIR}/libpepa/include/pEp/. "$D"/include/pEp

cp -arv ${INSTALLDIR}/pep/share/pEp/system.db "$D"/share/pEp

# SQlite CLI
cp -a ${INSTALLDIR}/sqlite3/bin/sqlite3 "$D"/bin

# Sequoia cmdline (optional above)
if [ -f ${INSTALLDIR}/sequoia/bin/sq ] ; then
  cp -a ${INSTALLDIR}/sequoia/lib/libsequoia_*.so* "$D"/lib/pEp
  cp -a ${INSTALLDIR}/sequoia/bin/sq "$D"/bin
  cp -a ${INSTALLDIR}/sequoia/bin/sqv "$D"/bin
else
  cp -a ${INSTALLDIR}/sequoia/lib/libsequoia_openpgp_ffi.so* "$D"/lib/pEp
fi

cp -a ${INSTALLDIR}/nettle/lib64/libnettle.so* "$D"/lib/pEp
cp -a ${INSTALLDIR}/nettle/lib64/libhogweed.so* "$D"/lib/pEp
cp -a ${INSTALLDIR}/gmp/lib/libgmp.so* "$D"/lib/pEp
cp -af ${INSTALLDIR}/llvm/lib/libc++.so* "$D"/lib/pEp
cp -af ${INSTALLDIR}/llvm/lib/libc++abi.so* "$D"/lib/pEp

cp -af ${INSTALLDIR}/boost/lib/libboost_*-mt-x64.so* "$D"/lib/pEp

# pEpJNI
cp -a ${INSTALLDIR}/pep/lib/libpEpJNI.a "$D"/lib
cp -a ${INSTALLDIR}/pep/lib/libpEpJNI.so "$D"/lib
cp -a ${INSTALLDIR}/pep/lib/pEp.jar "$D"/lib

# pEpPython
cp -a ${INSTALLDIR}/pep/share/pEp/pEp-*-py*.egg "$D"/share/pEp
cp -a ${INSTALLDIR}/pep/lib/pEp.cpython-*.so "$D"/lib
cp -a ${INSTALLDIR}/pep/lib/pEp.py "$D"/lib

# pEpJSON
cp -a ${INSTALLDIR}/pep/lib/libevent-2.0.so* "$D"/lib/pEp
cp -ar ${INSTALLDIR}/pep/share/pEp/html "$D"/share/pEp
# cp -ar ${WORKDIR}/pEpJSONServerAdapter/html "$D"/share/pEp/html
cp -a ${INSTALLDIR}/pep/bin/pEp-mini-json-adapter "$D"/bin
cp -a ${INSTALLDIR}/libevent/lib/libevent-2.0* "$D"/lib

# cp -a ${INSTALLDIR}/pep/bin/pEp-mJSON-enigmail "$D"/bin || true
# cp -a ${INSTALLDIR}/pep/bin/pep-json-server "$D"/bin || true

: > "$D"/share/pEp/VERSIONS.txt
for VER in ${INSTALLDIR}/*.ver ; do 
    _var="$(basename "$VER" .ver)"
    echo "$_var: $(cat "$VER")" >>"$D"/share/pEp/VERSIONS.txt
    eval "VER_${_var}=$(cat "$VER")"
done

find "$D"/lib -maxdepth 1 -type f -print -exec patchelf --set-rpath '$ORIGIN/pEp:$ORIGIN' {} \;
find "$D"/lib/pEp         -type f -print -exec patchelf --set-rpath '$ORIGIN' {} \;
find "$D"/bin -type f -print -exec patchelf --set-rpath '$ORIGIN/../lib/pEp:$ORIGIN/../lib' {} \;

while read DYLIB ; do
  patchelf --replace-needed libpython3.5m.so.rh-python35-1.0 libpython3.5m.so "$DYLIB"
done < <(find "$D" -type f -name \*.so\*)

ls -lh "$D"/*
du -sch "$D"

TARNAME="JNI-Adapter_r${D_REV}-${VER_pEp_JNI}+Sequoia-${VER_sq}+pEpEngine-${VER_pEp_engine}.tar.bz2"
tar -C "$D" -cJvf "${WORKDIR}/${TARNAME}" .

cd ${WORKDIR}/testout
rm -rf ${WORKDIR}/testout/*
tar -xf "${WORKDIR}/${TARNAME}"


ssh-add -l
scp -P 23099 "${WORKDIR}/${TARNAME}" root@haku.pep.foundation:/var/www/html/SWIFT/1.0.0/


LATEST="${TARNAME}"
cat <<EOF
LATEST="${TARNAME}"
wget "https://software.pep.foundation/jni-adapter/1.0.0/$LATEST"
mkdir -p /usr/local/pEp /usr/local/share/pEp
rm -rf /usr/local/pEp/*
tar -C /usr/local/pEp -xf "$LATEST"
ln -sf /usr/local/pEp/share/pEp/system.db /usr/local/share/pEp/system.db
EOF


# VER_asn1c=0.9.28
# VER_autoconf=2.69
# VER_automake=1.16.1
# VER_boost=1.70.0
# VER_capnp=0.7.0
# VER_cmake=3.14.3
# VER_cpptest=2.0.0
# VER_curl=7.64.1
# VER_gmp=6.1.2
# VER_libetpan=af2aad552dd0ecc6a943f78e5c77dc03ebbc0028
# VER_libiconv=1.16
# VER_libtool=2.4.6
# VER_llvm=0.8.0
# VER_m4=1.4.18
# VER_nettle=3.4
# VER_ninja=1.9.0
# VER_pEp_JNI=76a84d9a5f90
# VER_pEp_engine=04873e5d64f4
# VER_pEp_libadapter=1374a9d73a49
# VER_pEp_mini_JSON=6e5606c0e3ba
# VER_pEp_python=75d9f2875954
# VER_pkg_config=0.29.2
# VER_sq=v0.7.0-15-gec19b19
# VER_sqlite3=3280000

