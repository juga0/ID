FROM centos:centos7 

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ENV PEP_SYSTEM_DIR /opt/pEp/share

  # Create basic dirs
RUN mkdir -p ${BUILDROOT}
RUN mkdir -p ${INSTPREFIX}
RUN mkdir -p ${OUTDIR}
RUN mkdir -p ${PEP_SYSTEM_DIR}

  # Setup Env
ENV PATH = ${PATH}:${DIR_YML2}

WORKDIR /build

#RUN yum -y install patch git make yum-utils rpm-build rpmdevtools \
#       python27 rh-python35 openssl-devel zlib-devel libxml2-devel \
#       gmp gmp-devel sqlite-devel centos-release-scl devtoolset-8 \
#       devtoolset-8-gcc devtoolset-8-gcc-c++ python27 rh-python35 \
#       python-lxml mercurial libuuid libuuid-devel automake libtool \ 
#       cmake yum-utils rpm-build rpmdevtools && \
#    yum clean all
RUN yum -y install patch git cmake yum-utils rpm-build rpmdevtools \
                   openssl-devel zlib-devel epel-release \
	           libxml2-devel gmp gmp-devel sqlite-devel

RUN yum -y install centos-release-scl

RUN yum -y install devtoolset-8 devtoolset-8-gcc devtoolset-8-gcc-c++ \
                   python36 python36-lxml mercurial libuuid \
		   libuuid-devel automake libtool tcl which patchelf \ 
		   rustc cargo make pkg-config nettle-devel \
		   capnproto wget && \
                   yum clean all

RUN yum -y install java-1.7.0-openjdk java-1.7.0-openjdk-devel && \
    yum clean all
# yum -y install java-1.7.0-openjdk java-1.7.0-openjdk-devel
#yum -y install java-1.7.0-openjdk java-1.7.0-openjdk-devel



## RHEL/CentOS 6 64-Bit ##
#RUN wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm && \
#    rpm -ivh epel-release-6-8.noarch.rpm && \
#    yum -y install patchelf

RUN adduser -U -r -s /bin/sh pep-builder && \
    mkdir -p /home/pep-builder/.cargo

RUN chown -R pep-builder:pep-builder ${BUILDROOT} && \
    chown -R pep-builder:pep-builder ${INSTPREFIX} && \
    chown -R pep-builder:pep-builder ${OUTDIR} && \
    chown -R pep-builder:pep-builder ${PEP_SYSTEM_DIR} && \
    chown -R pep-builder:pep-builder /home/pep-builder

USER pep-builder

RUN export cmake_ver=3.14.3 && \
    export cmake_ver=3.14.5 && \
    cd ${BUILDROOT} && \
    wget https://github.com/Kitware/CMake/releases/download/v${cmake_ver}/cmake-${cmake_ver}.tar.gz \
        -O ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    rm ${BUILDROOT}/cmake-${cmake_ver}.tar.gz && \
    cd ${BUILDROOT}/cmake-${cmake_ver} && \
    source scl_source enable devtoolset-8 && \
    MAKEFLAGS=-j$(nproc) ./bootstrap --prefix=${INSTPREFIX}/cmake -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make -j$(nproc) && \
    make install && \
    echo \${cmake_ver}>${INSTPREFIX}/cmake.ver

RUN export ninja_ver=1.9.0 && \
    cd ${BUILDROOT} && \
    wget https://github.com/ninja-build/ninja/archive/v${ninja_ver}.tar.gz \
        -O ninja-${ninja_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf ninja-${ninja_ver}.tar.gz && \
    rm ninja-${ninja_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    cd ${BUILDROOT}/ninja-${ninja_ver} && \
    python configure.py --verbose --bootstrap && \
    mkdir -p ${INSTPREFIX}/ninja/bin && \
    install -m 755 ninja ${INSTPREFIX}/ninja/bin/ninja && \
    echo \${ninja_ver}>${INSTPREFIX}/ninja.ver



# Credits go to many, and in particular:
#   https://shaharmike.com/cpp/build-clang/

COPY llvm.sh /usr/bin/llvm.sh

RUN export llvm_ver=8.0.0 && \
    cd ${BUILDROOT} && \
    wget https://releases.llvm.org/${llvm_ver}/llvm-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/libcxx-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/libcxxabi-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/lld-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/lldb-${llvm_ver}.src.tar.xz && \
    wget https://releases.llvm.org/${llvm_ver}/cfe-${llvm_ver}.src.tar.xz && \
    mkdir -p ${BUILDROOT}/llvm && \
    tar -C ${BUILDROOT}/llvm -xf ${BUILDROOT}/llvm-${llvm_ver}.src.tar.xz && \
    rm ${BUILDROOT}/llvm-${llvm_ver}.src.tar.xz && \
    /bin/bash -c llvm.sh && \
    mkdir -p ${INSTPREFIX}/llvm && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/cmake/bin:${INSTPREFIX}/ninja/bin:$PATH && \
    export CC=$(which gcc) && \
    export CXX=$(which g++) && \
    export PYEXE=$(which python) && \
    mkdir -p ${BUILDROOT}/llvm/build/ && \
    cd ${BUILDROOT}/llvm/build/ && \
    cmake ../llvm-${llvm_ver}.src -GNinja \
      "-DCMAKE_CXX_COMPILER=$CXX" "-DCMAKE_C_COMPILER=$CC" "-DPYTHON_EXECUTABLE=$PYEXE" \
      -DLLVM_TARGETS_TO_BUILD=X86 \
      -DLLVM_INCLUDE_EXAMPLES=OFF -DLLVM_INCLUDE_DOCS=OFF \
      -DLIBCXX_ENABLE_SHARED=YES -DLIBCXX_ENABLE_STATIC=NO \
      -DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=NO \
      -DLIBCXX_CXX_ABI=libcxxabi \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=${INSTPREFIX}/llvm \
      -DLIBCXX_CXX_ABI=libcxxabi \
      -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${BUILDROOT}/llvm/build/include/c++/v1;${BUILDROOT}/llvm/llvm-${llvm_ver}.src/projects/libcxxabi/include" \
      -DLIBCXX_CXX_ABI_LIBRARY_PATH=${BUILDROOT}/llvm/build/lib && \
    ninja -j$(nproc) && \
    ninja install && \
    rm -rf ${BUILDROOT}/llvm && \
    echo "${llvm_ver}">${INSTPREFIX}/llvm.ver

# ============================================
# Things to compile with clang
# ============================================

# Install Sequoia dependency "capnproto" from source
#  -- following to https://capnproto.org/install.html, slightly adapted
COPY capnp_patch.sh /usr/bin/capnp_patch.sh

RUN export capnp_ver=0.7.0 && \
    cd ${BUILDROOT} && \
    #rm -rf  ${INSTPREFIX}/capnp && \
    mkdir -p ${INSTPREFIX}/capnp && \
    wget https://capnproto.org/capnproto-c++-${capnp_ver}.tar.gz \
        -O ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    # rm -rf ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    rm ${BUILDROOT}/capnproto-c++-${capnp_ver}.tar.gz && \
    cd ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    bash capnp_patch.sh



# ATTENTION ATTENTION ATTENTION
# =============================
#
# The line src/kj/filesystem-disk-test.c++ (tests fail)
#
# But, also that we disable sequoia-store in sequoia.
#
RUN source scl_source enable devtoolset-8 && \
    export capnp_ver=0.7.0 && \
    export PATH=${INSTPREFIX}/curl/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/capnproto-c++-${capnp_ver} && \
    ./configure --prefix=${INSTPREFIX}/capnp \
        CC=clang \
        CXX=clang++ "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++ " \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/capnp/lib -fuse-ld=lld" && \
    make -j$(nproc) all && \
    : > src/kj/filesystem-disk-test.c++ && \
    make -j$(nproc) check && \
    make install && \
    echo "${capnp_ver}">${INSTPREFIX}/capnp.ver



##
## QA:
##  * Need LD_RUN_PATH and LD_LIBRARY_PATH to lib-path of LLVM (or configure steps will fail)
##  * Need CC=clang and CXX=clang++
##  * CXXFLAGS needs: -I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++
##  * Repeat -stdlib=libc++ in both CXXFLAGS and "CXX=clang++ -stdlib=libc++"
##  * Add to LDFLAGS: -L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib
##  * Add any other local lib to LDFLAGS: -L<lib-path> -Wl,-rpath,<lib-path>
##  * Append to LDFLAGS: -Wl,-rpath,<lib-prefix>
##  * Append to LDFLAGS: -fuse-ld=lld
##  * Add local lib's pkgconfig directories to PKG_CONFIG_PATH
##
## Some docu for C++: https://libcxx.llvm.org/docs/UsingLibcxx.html
## https://wiki.musl-libc.org/building-llvm.html
## 
#
#
#
RUN export gmp_ver=6.1.2 && \
    cd ${BUILDROOT} && \
    wget https://gmplib.org/download/gmp/gmp-${gmp_ver}.tar.bz2 \
        -O ${BUILDROOT}/gmp-${gmp_ver}.tar.bz2 && \
    # rm -rf ${BUILDROOT}/gmp-${gmp_ver}
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/gmp-${gmp_ver}.tar.bz2 && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/gmp-${gmp_ver} && \
    ./configure --prefix=${INSTPREFIX}/gmp \
        --enable-cxx \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib -fuse-ld=lld" && \
    make -j$(nproc) && \
    make install && \
    echo "${gmp_ver}">${INSTPREFIX}/gmp.ver



RUN export nettle_ver=3.4 && \
    export nettle_ver=3.5 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/nettle/nettle-${nettle_ver}.tar.gz \
        -O ${BUILDROOT}/nettle-${nettle_ver}.tar.gz && \
    # rm -rf ${BUILDROOT}/nettle-${nettle_ver}
    tar -C ${BUILDROOT} -xf ${BUILDROOT}/nettle-${nettle_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/curl/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/nettle-${nettle_ver} && \
    ./configure --prefix=${INSTPREFIX}/nettle \
        --enable-x86-aesni \
        CC=clang CFLAGS="-I${INSTPREFIX}/gmp/include" \
        CXX=please-fail \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/nettle/lib64 -fuse-ld=lld"  && \
    make -j$(nproc) && \
    make install && \
    echo "${nettle_ver}">${INSTPREFIX}/nettle.ver


### Sequoia
RUN git clone https://gitlab.com/sequoia-pgp/sequoia.git -b pep-engine && \
    cd sequoia && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/cmake/bin:${INSTPREFIX}/curl/bin:${INSTPREFIX}/nettle/bin:${INSTPREFIX}/capnp/bin:${INSTPREFIX}/llvm/bin:$HOME/.cargo/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/nettle/lib64/pkgconfig && \
    pkg-config --libs-only-L nettle hogweed && \
   # export _Wl_rpath=$(pkg-config --libs-only-L nettle hogweed) && \
    #export _Wl_rpath=${_Wl_rpath//-L//-Wl,-rpath,/} && \
    export CC=clang && \
    export CFLAGS="-I${INSTPREFIX}/gmp/include $(pkg-config --cflags nettle hogweed) -I${INSTPREFIX}/pep/include" && \
    export CXX="clang++ -stdlib=libc++" && \
    export CXXFLAGS="-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++ -I${INSTPREFIX}/gmp/include $(pkg-config --cflags nettle hogweed) -I${INSTPREFIX}/pep/include" && \
    export LDFLAGS="-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib $(pkg-config --libs-only-L nettle hogweed) ${_Wl_rpath} -L${INSTPREFIX}/pep/lib -Wl,-rpath,${INSTPREFIX}/pep/lib -L"${sq_src}"/target/release -Wl,-rpath,${INSTPREFIX}/sequoia/lib -fuse-ld=lld" && \
    #export _rust_L="-Wl,-rpath,${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/gmp/lib ${_Wl_rpath} -Wl,-rpath,${INSTPREFIX}/pep/lib -Wl,-rpath,${INSTPREFIX}/sequoia/lib" && \
    #export _rust_L=${_rust_L-Wl,-C link-arg=-Wl,} && \
    export RUSTFLAGS=" -L${INSTPREFIX}/llvm/lib -L${INSTPREFIX}/gmp/lib $(pkg-config --libs-only-L nettle hogweed) -L${INSTPREFIX}/pep/lib" && \
    #source $HOME/.cargo/env && \
    make install PYTHON=disable PREFIX=$INSTPREFIX

### YML2
RUN cd $BUILDROOT && \
    wget https://fdik.org/yml2.tar.bz2 && \
    tar -xf yml2.tar.bz2 && \
    rm yml2.tar.bz2

### libetpan
RUN git clone https://github.com/fdik/libetpan $BUILDROOT/libetpan && \
    cd $BUILDROOT/libetpan && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    test -f configure || NOCONFIGURE=absolutely ./autogen.sh && \
    ./configure --prefix=${INSTPREFIX}/libetpan \
        --without-openssl --without-gnutls --without-sasl \
        --without-curl --without-expat --without-zlib \
        --disable-dependency-tracking \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -I${INSTPREFIX}/libiconv/include -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld" && \ 
    make -j$(nproc) && \
    make install && \
    echo "${libetpan_ver}">${INSTPREFIX}/libetpan.ver

### ASN1c Requirements
RUN export m4_ver=1.4.18 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/m4/m4-${m4_ver}.tar.gz \
        -O ${BUILDROOT}/m4-${m4_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf m4-${m4_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/m4-${m4_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${m4_ver}">${INSTPREFIX}/m4.ver





RUN export libtool_ver=2.4.6 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/libtool/libtool-${libtool_ver}.tar.gz \
        -O ${BUILDROOT}/libtool-${libtool_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf libtool-${libtool_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/libtool-${libtool_ver} && \
    ./configure --with-sysroot=${INSTPREFIX}/sysroot --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${libtool_ver}">${INSTPREFIX}/libtool.ver
    
    
    
    
RUN export autoconf_ver=2.69 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/autoconf/autoconf-${autoconf_ver}.tar.gz \ 
        -O ${BUILDROOT}/autoconf-${autoconf_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf autoconf-${autoconf_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/autoconf-${autoconf_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${autoconf_ver}">${INSTPREFIX}/autoconf.ver
    
    
RUN export automake_ver=1.16.1 && \
    cd ${BUILDROOT} && \
    wget https://ftp.gnu.org/gnu/automake/automake-${automake_ver}.tar.gz \
        -O ${BUILDROOT}/automake-${automake_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf automake-${automake_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/automake-${automake_ver} && \
    ./configure --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${automake_ver}">${INSTPREFIX}/automake.ver
    
    
RUN export pkg_config_ver=0.29.2 && \
    cd ${BUILDROOT} && \
    wget https://pkg-config.freedesktop.org/releases/pkg-config-${pkg_config_ver}.tar.gz \
        -O ${BUILDROOT}/pkg-config-${pkg_config_ver}.tar.gz && \
    tar -C ${BUILDROOT} -xf pkg-config-${pkg_config_ver}.tar.gz && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/pkg-config-${pkg_config_ver} && \
    ./configure  --with-internal-glib --with-sysroot=${INSTPREFIX}/sysroot --prefix=${INSTPREFIX}/autotools && \
    make -j$(nproc) && \
    make install && \
    echo "${pkg_config_ver}">${INSTPREFIX}/pkg_config.ver

### ASN1c
RUN git clone https://github.com/vlm/asn1c.git $BUILDROOT/asn1c && \
    cd $BUILDROOT/asn1c && \
    git checkout tags/v0.9.28 -b pep-engine && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/llvm/bin:${INSTPREFIX}/autotools/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/autotools/lib && \
    test -f configure || autoreconf -iv && \
    ./configure --prefix=${INSTPREFIX}/asn1c \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld" && \ 
    make -j$(nproc) && make install && \
    echo "${asn1c_ver}">${INSTPREFIX}/asn1c.ver

RUN export sqlite3_ver=3280000 && \
    export sqlite3_ver=3290000 && \
    cd ${BUILDROOT} && \
    wget https://sqlite.org/2019/sqlite-src-${sqlite3_ver}.zip \
        -O ${BUILDROOT}/sqlite-src-${sqlite3_ver}.zip && \
    rm -rf ${BUILDROOT}/sqlite-src-${sqlite3_ver} && \
    ( cd ${BUILDROOT} ; unzip -x ${BUILDROOT}/sqlite-src-${sqlite3_ver}.zip ) && \
    source scl_source enable devtoolset-8 && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib && \
    cd ${BUILDROOT}/sqlite-src-${sqlite3_ver} && \
    ./configure --prefix=${INSTPREFIX}/sqlite3 \
        --disable-tcl --disable-amalgamation --disable-readline --enable-debug \
        --enable-session \
        CC=clang \
        "CXX=clang++ -stdlib=libc++" "CXXFLAGS=-I${INSTPREFIX}/llvm/include/c++/v1 -stdlib=libc++" \
        "LDFLAGS=-L${INSTPREFIX}/llvm/lib -Wl,-rpath,${INSTPREFIX}/llvm/lib -fuse-ld=lld"  && \
    make -j$(nproc) && \
    make install && \
    echo "${sqlite3_ver}">${INSTPREFIX}/sqlite3.ver

### pEpEngine
#COPY pEpEngine.local.conf ${BUILDROOT}
RUN cd ${BUILDROOT} && \
    echo "PREFIX=$INSTPREFIX" >> local.conf && \
    echo 'SQLITE3_FROM_OS=""' >> local.conf && \
    echo "PER_MACHINE_DIRECTORY=$PEP_SYSTEM_DIR" >> local.conf && \
    echo "PER_SYSTEM_DIRECTORY=$PEP_SYSTEM_DIR" >> local.conf && \
    echo "YML2_PATH=${BUILDROOT}/yml2" >> local.conf && \
    echo "YML2_PROC=${BUILDROOT}/yml2/yml2proc" >> local.conf && \
    echo "ETPAN_LIB=-L${INSTPREFIX}/libetpan/lib" >> local.conf && \
    echo "ETPAN_INC=-I${INSTPREFIX}/libetpan/include" >> local.conf && \
    echo "#ASN1C=${INSTPREFIX}/asn1c/bin/asn1c" >> local.conf && \
    echo "ASN1C_INC=-I${INSTPREFIX}/asn1c/share/asn1c" >> local.conf && \
    echo "OPENPGP=SEQUOIA" >> local.conf && \
    echo "SEQUOIA_LIB=-L${INSTPREFIX}/lib" >> local.conf && \
    echo "SEQUOIA_INC=-I${INSTPREFIX}/include" >> local.conf && \
    echo "CC        = clang" >> local.conf && \
    echo "CXX       = clang++ -stdlib=libc++ -std=c++11" >> local.conf && \
    echo "CXXFLAGS  = -I${INSTPREFIX}/llvm/include/c++/v1 -I/opt/rh/devtoolset-8/root/usr/include/c++/8  -I/opt/rh/devtoolset-8/root/usr/include/c++/8/x86_64-redhat-linux/  -I${BUILDROOT}/pEpEngine/src -stdlib=libc++" >> local.conf && \
    echo "CPPFLAGS += -USYSTEM_DB -isystem ${INSTPREFIX}/asn1c/share/asn1c -I${INSTPREFIX}/sqlite3/include -I${INSTPREFIX}/libetpan/include -isystem ${INSTPREFIX}/pep/include -isystem /usr/include" >> local.conf && \
    echo "LDFLAGS  += -LINSTPREFIX}/llvm/lib -L${INSTPREFIX}/lib -L${INSTPREFIX}/libetpan/lib -L${INSTPREFIX}/sqlite3/lib -L${INSTPREFIX}/pep/lib -fuse-ld=lld -nostartfiles" >> local.conf

RUN hg clone https://pep.foundation/dev/repos/pEpEngine $BUILDROOT/pEpEngine && \
    cd $BUILDROOT/pEpEngine && \
    hg up sync && \
    export engine_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    mv ${BUILDROOT}/local.conf ${BUILDROOT}/pEpEngine/local.conf && \
    cat local.conf && \
    source scl_source enable devtoolset-8 && \
    export LC_ALL=en_US.UTF-8 && \
   # export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:/opt/rh/python27${BUILDROOT}/usr/lib64:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
    make && make install && \
    echo "Setup DB" && \
    make -C db install && \
    echo "${engine_ver}">${INSTPREFIX}/pEp_engine.ver
#    make db dbinstall 

### Test
RUN ls -l ${INSTPREFIX}/lib/libpEpEngine.so


## Adapters


### libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ $BUILDROOT/libpEpAdapter && \
    cd $BUILDROOT/libpEpAdapter && \
    echo "ENGINE_LIB_PATH=${INSTPREFIX}/lib" >> local.conf && \
    echo "ENGINE_INC_PATH=${INSTPREFIX}/include" >> local.conf && \
    source scl_source enable devtoolset-8 && \
    export LC_ALL=en_US.UTF-8 && \
   # export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
  make && \
  make install PREFIX=$INSTPREFIX



## JNI Adapter
USER root
RUN yum -y remove java-1.7.0-openjdk java-1.7.0-openjdk-devel && \
    yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
    yum clean all
RUN yum -y install time && yum clean all
USER pep-builder
COPY JNI.sh /usr/bin/JNI.sh
RUN hg clone https://pep.foundation/dev/repos/pEpJNIAdapter ${BUILDROOT}/pEpJNIAdapter && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    hg up -Cr sync && \
    hg purge -X local.conf --all --config extensions.purge= && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    export LC_ALL=en_US.UTF-8 && \
    export pepjni_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    #source scl_source enable devtoolset-8 && \
    #export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    #export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    #export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:/opt/rh/python27${BUILDROOT}/usr/lib64 && \
    #export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/sequoia/usr/share/pkgconfig:/opt/rh/python27${BUILDROOT}/usr/lib64/pkgconfig && \
    cd ${BUILDROOT}/pEpJNIAdapter/src && \
    mkdir -p ${INSTPREFIX}/home/java && \ 
    export PATH=${INSTPREFIX}/asn1c/bin:${INSTPREFIX}/sqlite3/bin:${INSTPREFIX}/llvm/bin:$PATH && \
    export LD_RUN_PATH=${INSTPREFIX}/llvm/lib && \
    export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${INSTPREFIX}/libetpan/lib:${BUILDROOT}/usr/lib64 && \
    export PKG_CONFIG_PATH=${INSTPREFIX}/pep/lib/pkgconfig:${INSTPREFIX}/share/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/capnp/lib/pkgconfig:${INSTPREFIX}/cpptest/lib/pkgconfig:${INSTPREFIX}/nettle/lib64/pkgconfig:${INSTPREFIX}/curl/lib/pkgconfig:${BUILDROOT}/usr/lib64/pkgconfig && \
    bash JNI.sh && \
    # make clean && \
    # For pEpEngine, do NOT override CFLAGS LDLIBS LDFLAGS here, && \
    #  instead add them to local.conf ( yes, home-cooked build-system :-/ ) && \
    make WARN= DEBUG= && \
    cd ${BUILDROOT}/pEpJNIAdapter/test && \
    make && \
    # make WARN= DEBUG= install && \
    cd ${BUILDROOT}/pEpJNIAdapter && \
    install -m 644 -t ${INSTPREFIX}/lib src/libpEpJNI.a && \
    install -m 755 -t ${INSTPREFIX}/lib src/libpEpJNI.so && \
    install -m 644 -t ${INSTPREFIX}/lib src/pEp.jar && \
    echo "${pepjni_ver}">${INSTPREFIX}/pEp_JNI.ver

### Test JNI (Currently fails, hence commented out...)
    #echo -e "Testing pEpJNIAdapter..." && \
    #cd $BUILDROOT/pEpJNIAdapter/test/Basic && \
    ##export LD_LIBRARY_PATH=${INSTPREFIX}/llvm/lib:${INSTPREFIX}/lib:${INSTPREFIX}/libetpan/lib:${INSTPREFIX}/nettle/lib64:/usr/lib64 && \
    #make && \
    #echo -e "Testing pEpJNIAdapter Finished\n"

COPY dist_tar.sh /build/dist_tar.sh
CMD ["/bin/bash", "-c", "/build/dist_tar.sh"]
