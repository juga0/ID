ARG DOCKER_REGISTRY_HOST
ARG CURRENT_DISTRO
ARG CURRENT_DISTRO_VERSION
FROM ${DOCKER_REGISTRY_HOST}/pep-${CURRENT_DISTRO}${CURRENT_DISTRO_VERSION}-sequoia:latest

ENV BUILDROOT /build
ENV INSTPREFIX /install
ENV OUTDIR /out
ARG PEP_MACHINE_DIR

ARG ENGINE_VERSION

USER root
RUN  apt-get update && apt-get install -y wget bzip2 && \
     rm -rf /var/lib/apt/lists/*

USER pep-builder

### YML2
RUN cd $BUILDROOT && \
    wget https://fdik.org/yml2.tar.bz2 && \
    tar -xf yml2.tar.bz2 && \
    rm yml2.tar.bz2


### libetpan
RUN git clone https://github.com/fdik/libetpan $BUILDROOT/libetpan && \
    cd $BUILDROOT/libetpan && \
    test -f configure || NOCONFIGURE=absolutely ./autogen.sh && \
    ./configure --prefix=${INSTPREFIX}/libetpan \
        --without-openssl --without-gnutls --without-sasl \
        --without-curl --without-expat --without-zlib \
        --disable-dependency-tracking && \
    make -j$(nproc) && \
    make install && \
    echo "${libetpan_ver}">${INSTPREFIX}/libetpan.ver


### ASN1c
RUN git clone https://github.com/vlm/asn1c.git $BUILDROOT/asn1c && \
    cd $BUILDROOT/asn1c && \
    git checkout tags/v0.9.28 -b pep-engine && \
    test -f configure || autoreconf -iv && \
    ./configure --prefix=${INSTPREFIX}/asn1c && \
    make -j$(nproc) && make install && \
    echo "${asn1c_ver}">${INSTPREFIX}/asn1c.ver


### pEpEngine
RUN cd ${BUILDROOT} && \
    echo "PREFIX=$INSTPREFIX" >> local.conf && \
    echo 'SQLITE3_FROM_OS=""' >> local.conf && \
    echo "PER_MACHINE_DIRECTORY=$PEP_MACHINE_DIR" >> local.conf && \
    echo "YML2_PATH=${BUILDROOT}/yml2" >> local.conf && \
    echo "YML2_PROC=${BUILDROOT}/yml2/yml2proc" >> local.conf && \
    echo "ETPAN_LIB=-L${INSTPREFIX}/libetpan/lib" >> local.conf && \
    echo "ETPAN_INC=-I${INSTPREFIX}/libetpan/include" >> local.conf && \
    echo "ASN1C=${INSTPREFIX}/asn1c/bin/asn1c" >> local.conf && \
    echo "ASN1C_INC=-I${INSTPREFIX}/asn1c/share/asn1c" >> local.conf && \
    echo "OPENPGP=SEQUOIA" >> local.conf && \
    echo "SEQUOIA_LIB=-L${INSTPREFIX}/lib" >> local.conf && \
    echo "SEQUOIA_INC=-I${INSTPREFIX}/include" >> local.conf && \
    echo "LDFLAGS  += -L${INSTPREFIX}/lib -L${INSTPREFIX}/libetpan/lib -L${INSTPREFIX}/pep/lib -nostartfiles" >> local.conf


RUN hg clone https://pep.foundation/dev/repos/pEpEngine $BUILDROOT/pEpEngine && \
    cd $BUILDROOT/pEpEngine && \
    hg up ${ENGINE_VERSION} && \
    export engine_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    mv ${BUILDROOT}/local.conf ${BUILDROOT}/pEpEngine/local.conf && \
    cat local.conf && \
    export LC_ALL=en_US.UTF-8 && \
    export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    make && make install

# Create systemdb (need to be root depending on the path)
USER root
RUN cd $BUILDROOT/pEpEngine && \
    export engine_ver=$(x=$(hg id -i) ; echo ${x/+/}) && \
    export LC_ALL=en_US.UTF-8 && \
    export PKG_CONFIG_PATH=$INSTPREFIX/share/pkgconfig/ && \
    echo "Setup DB" && \
    make -C db install && \
    echo "${engine_ver}">${INSTPREFIX}/pEp_engine.ver
USER pep-builder
